# 基于MQTT的物联网管理平台
本项目是基于MQTT（消息队列遥测传输）协议的物联网（Internet of Things, IoT）项目、设备在线管理平台，源于本科期间的课程设计项目，
目前前端主要引入了如jQuery、BootStrap以及mqttws3.1等一些必要js文件， 后端主要以Java Server Pages（.jsp）的方式构建，容器为Tomcat v7.0。
## 基本信息
 1. 开发软件：`Eclipse Java EE IDE. Version: 2018-09 (4.9.0) Build id: 20180917-1800`；
 2. 使用容器：`Apache Tomcat Version 7.0`；
 3. 使用数据库：`5.6.29 MySQL Community Server`；
 4. 使用语言：`Java`、`JavaScript`等涉及JSP开发的相关语言；
## 建议的重新部署方式
 1. 第一步：建议使用Java Web开发相关的IDE如：`Eclipse EE`或`MyEclipse`；
 2. 第二步：使用IDE新建一个Web项目；
 3. 第三步：JSP文件可以直接复制粘贴进WebRoot或WebContent文件夹下； 
 对于JavaScript与css文件，请将Js文件夹放在WebContent(Eclipse)或WebRoot(MyEclipse)下，保证JSP文件能引用到相关的js文件；
 但对于Java类，请在相关的文件夹下新建相关的包之后，再把具体的类复制进去；
 4. 第四步：引入必要的jar库，本项目至少需要引入4个jar包。在Eclipse中，可以通过  **右键项目文件夹-Properties-Java Bulid Path-Libraries-Add External JARs...**  添加以下必要jar包，如果不想自己搜索下载，可以直接从`/WebContent/WEB-INF/lib`目录下导入：
  - commons-fileupload-1.2.jar
  - commons-io.jar
  - fastjson-1.1.24.jar
  - mysql-connector-java-5.1.25.jar
 5. 第五步：对于MyEclipse可以直接使用IDE集成的Tomcat容器直接部署，Eclipse EE则要新建Server，具体方法可以参考[这里](https://blog.csdn.net/qq_37359142/article/details/57131075)；
 6. 第六步：关于数据库，建议使用Navicat先对MySQL数据库进行添加数据库`iotdb`操作，
 再将源码目录下的`iotdb.sql`文件在该数据库下运行即可。
## 必要的文件
**\*请注意：** 
 1. MySQL下的iotdb数据库是系统能正常运行的必要条件不可缺少！
 2. `/WebContent/WEB-INF/lib`目录下的jar包是必要且不可缺少的，否则会导致部分功能出错。
 2. `/WebContent/Js`文件夹为必要文件夹，当中的JS文件和CSS文件可以根据自己的喜好添加，但不可删除；
 3. 本项目中的MQTT实现是借用[公有云的资源](http://www.mqtt-dashboard.com/)，如果确实要部署，建议部署到自己的云端服务器上。