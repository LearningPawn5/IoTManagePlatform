<%@page import="iot.dao.UtilsDao"%>
<%@ page language="java" import="java.util.*,iot.bean.*"
	pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String UserType = session.getAttribute("loginusertype").toString();
String UserName = session.getAttribute("loginusername").toString();
%>	

<jsp:include page="head.jsp"></jsp:include>

<div class="container">
	<ol class="breadcrumb">
		<li><a href="index.jsp">Home</a></li>
		<li><a href="GateServlet">网关管理</a></li>
		<li class="active">编辑网关</li>
	</ol>

<!-- Bootstrap 面板（Panels）-->
	<div class="panel panel-default">
		<div class="panel-heading">编辑网关信息</div>
		<div class="panel-body">
		<jsp:useBean id="gate" class="iot.bean.Tgate" scope="request">
		       <jsp:setProperty name="gate" property="*" />
		</jsp:useBean>

			<!-- form-horizontal：水平表单，元素水平排列  ?action=updateSave 带参数时 必须是post-->
			<form class="form-horizontal" action="GateServlet?action=updateSave" method="post" data-role="form">
				<div class="form-group">
				   <!--  col-md-4：表示12列占用4列，可以控制宽度（bootstrap网格系统） -->
					<label class="col-md-4 control-label">网关代码：</label>
					<div class="col-md-4">
						<input class="form-control" name="gateid" id="gateid" value="<%=gate.getGateid() %>" placeholder="自定义网关代码，英文数字的集合" type="text" required>
					</div>
					<input type="hidden" name="id"  value="<%=gate.getId() %>"  id="id" >
				</div>
				<div class="form-group">
				   <!--  col-md-4：表示12列占用4列，可以控制宽度（bootstrap网格系统） -->
					<label class="col-md-4 control-label">项目ID：</label>
					<div class="col-md-4">
						<select id="pid" name="pid" class="form-control" title="请选择您的项目ID">
							<%
								UtilsDao ud = new UtilsDao();
								Map<Long, String> values = ud.relUserAll();
								Long uid = 0L;
								for (Map.Entry<Long, String> entry : values.entrySet()) {
									uid = UserName.equals(entry.getValue())?entry.getKey():uid;
								}
								//values = new HashMap<Long, String>();
								if(UserType.equals("管理员")) {
									values = ud.relProjectAll();
								} else {
									values = ud.relProjectByUid(uid);
								}
								ud.closedb();
								for (Map.Entry<Long, String> entry : values.entrySet()) {
										out.println("<option value='" + entry.getKey() + "' "+(gate.getPid()==entry.getKey()?"selected":"")+">" + entry.getValue() + "</option>");
								}
							%>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">网关名称：</label>
					<div class="col-md-4">
						<input class="form-control" name="gatename" id="gatename" value="<%=gate.getGatename() %>" placeholder="请输入设备类型名称" type="text" required>
					</div>
				</div>
				<div class="form-group">
				</div>
				<div class="form-group">
				    <label class="col-md-4 control-label"></label>
				    <div class="col-md-4">
				         <button type="submit" class="btn btn-default">保存</button>
				         <a class="btn btn-default" href="GateServlet">返回</a>
				    </div>
				</div>
			</form>
		</div>
	</div>
</div>
<jsp:include page="foot.jsp"></jsp:include>
