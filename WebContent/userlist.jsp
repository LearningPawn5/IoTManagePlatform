<%@ page language="java" import="java.util.*,iot.bean.*"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
String basePath = 
request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String UserType = session.getAttribute("loginusertype").toString();
%>
<jsp:include page="head.jsp"></jsp:include>
<div class="container">
	<ol class="breadcrumb">
		<li><a href="index.jsp">Home</a></li>
		<li class="active">用户管理</li>
	</ol>
	<!-- 字体图标和bootstrap按钮 -->
	<div class="btn-toolbar">
	<%if(UserType.equals("管理员")){%>
		<a class="btn btn-primary" href="useradd.jsp">
			<i class="glyphicon glyphicon-plus"></i>添加
		</a>
	<%} %>
		<%
			//取出上回输入的查询条件
			String fieldname="";
			String keyword="";
			if(request.getAttribute("fieldname")!=null && request.getAttribute("keyword")!=null){
				fieldname=request.getAttribute("fieldname").toString();
				keyword=request.getAttribute("keyword").toString();
			}
		%>
		<!-- form-inline:内联表单，表单内元素会水平排列； pull-right：向右浮动 -->
		<form action="UserServlet" class="form-inline pull-right">
			<div class="form-group">
				<label for="fieldname">选择:</label> 
				<select id="fieldname"
					name="fieldname" class="form-control" onchange="onChg(this)">
					<option value="id" <%=fieldname.equals("id")?"selected":""%>>用户 ID</option>
					<option value="username"
						<%=fieldname.equals("username")?"selected":""%>>用户名</option>
					<option id="usertype" value="usertype"
						<%=fieldname.equals("usertype")?"selected":""%>>用户类型</option>
				</select>
				<script type="text/javascript">
				function onChg(obj) {
					if(obj.selectedIndex == 2){
						document.getElementById("keyword").value = "";
						document.getElementById("keyword").style.display = "none";
						document.getElementById("keyword-group").style.display = "inline";
					} else {				
						document.getElementById("keyword").value = "";							
						document.getElementById("keyword").style.display = "inline";
						document.getElementById("keyword-group").style.display = "none";
					}
				}
				function onKeyChg(obj) {
					if(obj.selectedIndex == 0){
						document.getElementById("keyword").value = "普通用户";
					} else {										
						document.getElementById("keyword").value = "管理员";
					}
				}
				</script>
			</div>
			<div class="form-group">
				<input type="text" name="keyword" value="<%=keyword%>" id="keyword" <%=keyword.equals("普通用户")||keyword.equals("管理员")?"style='display: none;'":""%>
					class="form-control" placeholder="请输入关键词"> 
				<select	id="keyword-group" <%=keyword.equals("普通用户")||keyword.equals("管理员")?"":"style='display: none;'"%> onchange="onKeyChg(this)" name="keyword-group" class="form-control">
					<option value="普通用户" <%=keyword.equals("普通用户")?"selected":""%>>普通用户</option>
					<option value="管理员" <%=keyword.equals("管理员")?"selected":""%>>管理员</option>
				</select>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-default">查询</button>
			</div>
		</form>
	</div>
	<!-- table-striped：奇偶行颜色不同，基本格式见文档 -->
	<table class="table table-striped">
		<thead>
			<tr>
				<th>ID</th>
				<th>用户名</th>
				<th>用户类型</th>
				<th>操作</th>
			</tr>
		</thead>
		<tbody>
			<%
				@SuppressWarnings("unchecked")
				//users存放查询结果，以集合方式存储，每个成员为值JavaBean
				ArrayList<Tuser> users = (ArrayList<Tuser>) request.getAttribute("users");
				if (users != null) {
					for (int i = 0; i < users.size(); i++) { //依次提取集合中的每一个成员
						Tuser user = users.get(i);
			%>
			<tr>
				<td><%=user.getId()%></td>
				<td><%=user.getUsername()%></td>
				<td><%=user.getUsertype()%></td>
				<td>
				<%if(UserType.equals("管理员")||user.getUsername().equals(session.getAttribute("loginusername"))){%>
				<a href="UserServlet?action=update&id=<%=user.getId()%>">
					<i class="glyphicon glyphicon-pencil"></i>
				</a>
				<%if(UserType.equals("管理员")||!user.getUsername().equals(session.getAttribute("loginusername"))){%>
				<a href="javascript:void(0)" onclick="javascript:if(confirm('是否确认要删除？'))location.href='UserServlet?action=delete&id=<%=user.getId()%>'">
						<i class="glyphicon glyphicon-trash"></i>
				</a>
				<%}} %>
				</td>
			</tr>
			<%
				}
			}
			%>
		</tbody>
	</table>
	<!-- 下面是控制分页控件，必须要是ul元素才行 -->
	<ul id='bp-element'></ul>
	<%
		String msg="";
		if( request.getAttribute("msg") != null ){
			msg=request.getAttribute("msg").toString();
		}
		if(msg.length()>0){
		//显示提示信息
	%>

	<!-- Bootstrap 警告（Alerts） -->
	<div class="alert alert-info" id="statusbar" data-role="alert"><%=msg%></div>
	<%
		}
	%>
</div>
<%
	//获取当前页面和总页数
String currentPage=request.getAttribute("currentPage").toString();
String total=request.getAttribute("total").toString();
%>
<script type="text/javascript">
$(function(){
 var element = $('#bp-element');// bp-element上方刚定义的ul元素
 options = {
 bootstrapMajorVersion:3, //对应的bootstrap版本
 currentPage: <%=currentPage%>, //当前请求页面，获取从后台传过来的值
 numberOfPages: 5, //控件显示的页码数。
 totalPages:<%=total%>
	, //总页数，获取从后台传过来的值
			shouldShowPage : true,//某操作按钮是否显示
			itemTexts : function(type, page, current) {
				//控制页眉（操作按钮）的显示样式，将操作按钮上的英文显示为中文
				switch (type) {
				case "first":
					return "首页";
				case "prev":
					return "上一页";
				case "next":
					return "下一页";
				case "last":
					return "末页";
				case "page":
					return page;
				}
			},
			//点击事件
			onPageClicked : function(event, originalEvent, type, page) {

				location.href = "UserServlet?page=" + page + "&fieldname=<%=fieldname%>&keyword=<%=keyword%>";
			}
		};
		element.bootstrapPaginator(options);
	});
</script>
<jsp:include page="foot.jsp"></jsp:include>
