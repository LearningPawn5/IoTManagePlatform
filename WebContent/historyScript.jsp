<%@page import="iot.bean.ScriptNeedInfo"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
@SuppressWarnings("unchecked")
List<ScriptNeedInfo> infoList = (ArrayList<ScriptNeedInfo>)session.getAttribute("infoList");
%>

<script type="text/javascript">
$(function() {
	var picker1;
	var picker2;
});
<% 
if(infoList != null)
for(ScriptNeedInfo info : infoList){ 
	String code = info.getCode();
	String desc = info.getDescript();
	String js_dtp_format = "$(function() {\n	picker1 = $('#"+code+"-dtp1').datetimepicker({\n		format : 'YYYY-MM-DD HH:mm:ss',\n		locale : moment.locale('zh-cn'),\n	});\n	picker2 = $('#"+code+"-dtp2').datetimepicker({\n		format : 'YYYY-MM-DD HH:mm:ss',\n		locale : moment.locale('zh-cn')\n	});// 动态设置最小值\n	picker1.on('dp.change', function(e) {\n		picker2.data('DateTimePicker').minDate(e.date);\n	});// 动态设置最大值\n	picker2.on('dp.change', function(e) {\n		picker1.data('DateTimePicker').maxDate(e.date);\n	});\n});\n";
	String js_chart_format = "$(function() {var "+code+"Chart = echarts.init(document.getElementById('"+code+"-chart'));\ninitChart("+code+"Chart, '"+desc+"', '"+code+"');\n$('#"+code+"-que').click(		function() {			$.ajax({				type : 'post',				url : 'HistoryDataServlet?action=query&starttime='						+ $('#"+code+"-dtp1').find('input').val()						+ '&endtime='						+ $('#"+code+"-dtp2').find('input').val(),				success : function(data) {					console.log('"+code+" log: query OK. And data: ' + data);					jsondata = JSON.parse(data);					times = [];					humis = [];					temps = [];					//参数设置\n					option = {						title : { //标题组件\n							text : '"+desc+"折线图'						},						tooltip : { //提示框组件\n							trigger : 'axis'						},						legend : { //图例组件\n							data : [ '温度', '湿度' ]						},						grid : { //直角坐标系内绘图网格\n							left : '3%',							right : '4%',							bottom : '3%',							containLabel : true						},						toolbox : { //工具栏\n							feature : {								saveAsImage : {}							}						},						xAxis : { //直角坐标系 grid 中的 x 轴\n							type : 'category',							boundaryGap : false,							data : jsondata.time						},						yAxis : { //直角坐标系 grid 中的 y 轴\n							type : 'value'						},						series : [ //系列列表\n						{							name : '温度',							type : 'line',							data : jsondata.temp						}, {							name : '湿度',							type : 'line',							data : jsondata.humi						}, ]					};					$('#"+code+"-chart').css('display', 'block');					"+code+"Chart.setOption(option); //参数设置方法\n				},				error : function(result) {					$('#statusbar').attr('class', 'alert alert-warning');					$('#statusbar').text('连接数据库服务器失败！ ');				}			});		});";	
	String js_keyfun_format = "var "+code+"flag = false;\nvar "+code+"timer;\n$('#"+code+"-rt').click(function() {\n	"+code+"flag = !"+code+"flag;\n	$('#"+code+"-rt').toggleClass('btn-primary btn-success');\n	"+code+"timer = window.setInterval(function() {\n		if (!"+code+"flag) {\n			window.clearInterval("+code+"timer);\n			"+code+"timer = 0;\n			return;\n		} else\n			$.ajax({\n				type : 'post',\n				url : 'HistoryDataServlet?action=query',\n				success : function(data) {\n					console.log('"+code+" log: Realtime query OK. And data: ' + data);\n					jsondata = JSON.parse(data);\n					times = [];\n					humis = [];\n					temps = [];\n					// 参数设置\n					option = {\n						title : { // 标题组件\n							text : '"+desc+"折线图'\n						},\n						tooltip : { // 提示框组件\n							trigger : 'axis'\n						},\n						legend : { // 图例组件\n							data : [ '温度', '湿度' ]\n						},\n						grid : { // 直角坐标系内绘图网格\n							left : '3%',\n							right : '4%',\n							bottom : '3%',\n							containLabel : true\n						},\n						toolbox : { // 工具栏\n							feature : {\n								saveAsImage : {}\n							}\n						},\n						xAxis : { // 直角坐标系 grid 中的 x 轴\n							type : 'category',\n							boundaryGap : false,\n							data : jsondata.time\n						},\n						yAxis : { // 直角坐标系 grid 中的 y 轴\n							type : 'value'\n						},\n						series : [ // 系列列表\n						{\n							name : '温度',\n							type : 'line',\n							data : jsondata.temp\n						}, {\n							name : '湿度',\n							type : 'line',\n							data : jsondata.humi\n						}, ]\n					};\n					$('#"+code+"-chart').css('display', 'block');\n					"+code+"Chart.setOption(option); // 参数设置方法\n				},\n				error : function(result) {\n					$('#statusbar').attr('class', 'alert alert-warning');\n					$('#statusbar').text('连接数据库服务器失败！ ');\n				}\n			});\n	}, 1500);\n});\n});";	
	out.println(String.format(js_dtp_format, code, code, code, code));
	out.println(js_chart_format);
	out.println(js_keyfun_format);
} %>
</script>
