<%@page import="iot.dao.UtilsDao"%>
<%@ page language="java" import="java.util.*,iot.bean.*"
	pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String UserType = session.getAttribute("loginusertype").toString();
String UserName = session.getAttribute("loginusername").toString();
%>

<jsp:include page="head.jsp"></jsp:include>

<div class="container">
	<ol class="breadcrumb">
		<li><a href="#">Home</a></li>
		<li class="active">编辑设备</li>
	</ol>

<!-- Bootstrap 面板（Panels）-->
	<div class="panel panel-default">
		<div class="panel-heading">编辑设备信息</div>
		<div class="panel-body">
		<jsp:useBean id="gatedevice" class="iot.bean.Tgatedevice" scope="request">
		       <jsp:setProperty name="gatedevice" property="*" />
		</jsp:useBean>

			<!-- form-horizontal：水平表单，元素水平排列  ?action=add 带参数时 必须是post-->
			<form class="form-horizontal" action="GateDeviceServlet?action=updateSave" method="post" data-role="form">
				<div class="form-group">
					<label class="col-md-4 control-label">网关ID：</label>
					<div class="col-md-4">
					    <%
								UtilsDao ud = new UtilsDao();
								Long uid = ud.getUidByUserName(UserName);
								Map<Long, String> values = new HashMap<Long, String>();
								if(UserType.equals("管理员")) {
									values = ud.relGateAll();
								} else {
									values = ud.relGateByUid(uid);
								}
								ud.closedb();
								boolean flag = (values.size()>0);
								if (flag)
									out.println("<select id='gid' name='gid' class='form-control' title='请选择您的网关'>");
								for (Map.Entry<Long, String> entry : values.entrySet()) {
									out.println("<option value='" + entry.getKey() + "' "+(gatedevice.getGid().equals(entry.getKey())?"selected":"")+">" + entry.getValue() + "</option>");
								}
								if (!flag)
									out.println("<a class='btn btn-default' href='gateadd.jsp'> <i class='glyphicon glyphicon-plus'></i> 您还没有创建网关，点击添加网关		</a>");
								else
									out.println("</select>");
							%>
				    </div>
				    <input type="hidden" name="id"  value="<%=gatedevice.getId() %>"  id="id" >
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">设备ID：</label>
					<div class="col-md-4">
					    <select id="did" name="did" class="form-control" title="请选择设备">
							<%
								values = ud.relDeviceAll();
								ud.closedb();
								for (Map.Entry<Long, String> entry : values.entrySet()) {
									out.println("<option value='" + entry.getKey() + "' "+(gatedevice.getDid().equals(entry.getKey())?"selected":"")+">" + entry.getValue() + "</option>");
								}
							%>
						</select>
					</div>
				</div>
				<div class="form-group">
				   <!--  col-md-4：表示12列占用4列，可以控制宽度 -->
					<label class="col-md-4 control-label">终端设备名称：</label>
					<div class="col-md-4">
						<input class="form-control" name="clientdevicename" id="clientdevicename" placeholder="请输入终端设备名称" value="<%=gatedevice.getClientdevicename() %>" type="text" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">终端设备编号：</label>
					<div class="col-md-4">
					    <input class="form-control" name="clientdeviceid" id="clientdeviceid" placeholder="请输入终端设备编号" value="<%=gatedevice.getClientdeviceid() %>" type="text" >
				    </div>
				</div>
				<div class="form-group">
				    <label class="col-md-4 control-label"></label>
				    <div class="col-md-4">
				         <button type="submit" class="btn btn-default">保存</button>
				         <a class="btn btn-default" href="GateDeviceServlet">返回</a>
				    </div>
				</div>
			</form>
			
		</div>
	</div>

</div>


<jsp:include page="foot.jsp"></jsp:include>
