<%@page import="iot.dao.UtilsDao"%>
<%@ page language="java" import="java.util.*,iot.bean.*"
	pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String UserType = session.getAttribute("loginusertype").toString();
String UserName = session.getAttribute("loginusername").toString();
%>	

<jsp:include page="head.jsp"></jsp:include>

<div class="container">
	<ol class="breadcrumb">
		<li><a href="#">Home</a></li>
		<li><a href="ProjectServlet">监控项目管理</a></li>
		<li class="active">添加项目</li>
	</ol>

<!-- Bootstrap 面板（Panels）-->
	<div class="panel panel-default">
		<div class="panel-heading">添加项目信息</div>
		<div class="panel-body">

			<!-- form-horizontal：水平表单，元素水平排列  ?action=add 带参数时 必须是post-->
			<form class="form-horizontal" action="ProjectServlet?action=add" method="post">
				<div class="form-group" <%=UserType.equals("管理员")?"":"style='display:none'" %>>
				   <!--  col-md-4：表示12列占用4列，可以控制宽度（bootstrap网格系统） -->
					<label class="col-md-4 control-label">用户ID：</label>
					<div class="col-md-4">
						<select id="uid" name="uid" class="form-control" title="请选择用户" required>
							<%
								UtilsDao ud = new UtilsDao();
								Map<Long, String> values = ud.relUserAll();
								ud.closedb();
								for (Map.Entry<Long, String> entry : values.entrySet()) {
									boolean flag = UserName.equals(entry.getValue());
									if (UserType.equals("管理员")||flag)
										out.println("<option value='" + entry.getKey() + "' "+(flag?"selected":"")+">" + entry.getValue() + "</option>");
								}
							%>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">项目名称：</label>
					<div class="col-md-4">
						<input class="form-control" name="projectname" id="projectname" placeholder="请输入项目名称" type="text" required>
					</div>
				</div>
				<div class="form-group">
				</div>
				<div class="form-group">
				    <label class="col-md-4 control-label"></label>
				    <div class="col-md-4">
				         <button type="submit" class="btn btn-default">保存</button>
				         <a class="btn btn-default" href="ProjectServlet">返回</a>
				    </div>
				</div>
			</form>
		</div>
	</div>

</div>
<jsp:include page="foot.jsp"></jsp:include>
