<%@page import="iot.dao.UtilsDao"%>
<%@ page language="java" import="java.util.*,iot.bean.*"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
	String UserType = session.getAttribute("loginusertype").toString();
	String UserName = session.getAttribute("loginusername").toString();
%>
<jsp:include page="head.jsp"></jsp:include>
<!-- 后面整合到head.jsp
<link href="js/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css" rel="stylesheet">
<script src="js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="js/jquery.cookie.js"></script>
<script src="js/radialIndicator-master/radialIndicator.js"></script>

<script
	src="https://cdnjs.cloudflare.com/ajax/libs/paho-mqtt/1.0.1/mqttws31.min.js"
	type="text/javascript"></script> -->
<script src="js/self/mqttsend.js" charset="utf-8"></script>
<script src="js/self/glue-panels.js" charset="utf-8"></script>
<jsp:include page="historyScript.jsp"></jsp:include>
<script type="text/javascript">

$(function(){
	client.connect(options); // 连接mqtt服务器
		//$('#container').css('display', 'none');
		
});
</script>
<div class="container">
	<ol class="breadcrumb">
		<li><a href="index.jsp">Home</a></li>
		<li class="active">历史数据</li>
	</ol>

	<div class="btn-toolbar">
		<a class="btn btn-primary" href="GateDeviceServlet"> <i
			class="glyphicon glyphicon-th-list"></i> 监控设备管理
		</a>
		<form action="HistoryDataServlet" class="form-inline pull-right">
			<!--<div class="form-group">
				<label for="pid" class="control-label">项目ID：</label>
				<%
					UtilsDao ud = new UtilsDao();
					Long uid = ud.getUidByUserName(UserName);
					Map<Long, String> values = new HashMap<Long, String>();
					if(UserType.equals("管理员")) {
						values = ud.relProjectAll();
					} else {
						values = ud.relProjectByUid(uid);
					}
					ud.closedb();
					boolean flag = (values.size()>0);
					if (flag)
						out.println("<select id='pid' name='pid' class='form-control' title='请选择您的项目ID'>");
					for (Map.Entry<Long, String> entry : values.entrySet()) {
						out.println("<option value='" + entry.getKey() + "'>" + entry.getValue() + "</option>");
					}
					if (!flag)
						out.println("<br/><a class='btn btn-default' href='projectadd.jsp'> <i class='glyphicon glyphicon-plus'></i> 您还没有创建项目，点击添加项目 </a>");
					else
						out.println("</select>");
				%>
			</div>-->
			<div class="form-group">
				<label for="gid" class="control-label">网关ID：</label>
				<%
				Long pid = 0L;
				//values = new HashMap<Long, String>();
				if(UserType.equals("管理员")) {
					values = ud.relGateAll();
				} else {
					values = ud.relGateByUid(uid);
				}
				ud.closedb();
				boolean Gflag = (values.size()>0);
				if (Gflag)
					out.println("<select id='gid' name='gid' class='form-control' title='请选择您的网关'>");
				for (Map.Entry<Long, String> entry : values.entrySet()) {
					out.println("<option value='" + entry.getKey() + "' "+((entry.getKey()==request.getAttribute("gid"))?"selected":"")+">" + entry.getValue() + "</option>");
				}
				if (!Gflag)
					out.println("<br/><a class='btn btn-default' href='gateadd.jsp'> <i class='glyphicon glyphicon-plus'></i> 您还没有创建网关，点击添加网关 </a>");
				else
					out.println("</select>");
				%>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-default" <%=flag|Gflag?"":"disabled" %>>立即切换</button>
			</div>
		</form>
	</div>
	<br />
	<%
		@SuppressWarnings("unchecked")
		List<String> newForms = (List<String>) request.getAttribute("newForms");
		if (newForms != null)
			for (String newForm : newForms) {
				out.println(newForm);
			}
	%>
</div>
<div class="container">
	<div class="alert alert-info" id="statusbar" data-role="alert"></div>
</div>
<jsp:include page="foot.jsp"></jsp:include>