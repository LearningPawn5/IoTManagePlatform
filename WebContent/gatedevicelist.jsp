<%@page import="iot.dao.UtilsDao"%>
<%@ page language="java" import="java.util.*,iot.bean.*"
	pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String UserType = session.getAttribute("loginusertype").toString();
String UserName = session.getAttribute("loginusername").toString();
%>

<jsp:include page="head.jsp"></jsp:include>

<div class="container">
	<ol class="breadcrumb">
		<li><a href="index.jsp">Home</a></li>
		<li class="active">监控设备管理</li>
	</ol>

<!-- 字体图标和bootstrap按钮 -->	
<div class="btn-toolbar">
		<a class="btn btn-primary" href="gatedeviceadd.jsp"> <i
			class="glyphicon glyphicon-plus"></i>添加
		</a>
	

<%
//取出上回输入的查询条件

String fieldname="";
String keyword="";

if(request.getAttribute("fieldname")!=null && request.getAttribute("keyword")!=null){
	fieldname=request.getAttribute("fieldname").toString();
	keyword=request.getAttribute("keyword").toString();
}

%>
       <!--  form-inline:内联表单，表单内元素会水平排列； pull-right：向右浮动 -->
		<form action="GateDeviceServlet" class="form-inline pull-right">
			<div class="form-group">
				<label for="fieldname">选择:</label> 
				<select id="fieldname" name="fieldname" class="form-control">
					<option value="id" <%=fieldname.equals("id")?"selected":"" %> >ID</option>
					<option value="gid" <%=fieldname.equals("gid")?"selected":"" %> >网关ID</option>
					<option value="did" <%=fieldname.equals("did")?"selected":"" %> >设备ID</option>
					<option value="clientdeviceid" <%=fieldname.equals("clientdeviceid")?"selected":"" %> >终端设备编号</option>
					<option value="clientdevicename" <%=fieldname.equals("clientdevicename")?"selected":"" %> >终端设备名称</option>
				</select>
			</div>
            <div class="form-group">
                <input type="text" name="keyword" value="<%=keyword %>" id="keyword" class="form-control" placeholder="请输入关键词">
            </div>
			<div class="form-group">
				<button type="submit" class="btn btn-default">查询</button>
			</div>
		</form>
		

	</div>
	
	
<!-- table-striped：奇偶行颜色不同，基本格式见文档 -->
	<table class="table table-striped">
		<thead>
			<tr>
				<th>ID</th>
				<th>网关ID</th>
				<th>设备ID</th>
				<th>终端设备名称</th>
				<th>终端设备编号</th>
				<th>添加时间</th>
				<th>操作</th>
			</tr>
		</thead>
		<tbody>
			<%
				@SuppressWarnings("unchecked")
				//users存放查询结果，以数组方式存储，每个成员为值JavaBean
				ArrayList<Tgatedevice> gatedevices = (ArrayList<Tgatedevice>) request.getAttribute("gatedevices");
				UtilsDao ud = new UtilsDao();
				Long uid = ud.getUidByUserName(UserName);
				if (gatedevices != null) {
				for (int i = 0; i < gatedevices.size(); i++) { //依次提取数组中的每一个成员
					Tgatedevice gatedevice = gatedevices.get(i);
					if (UserType.equals("管理员") || ud.getUidByGid(gatedevice.getGid()) == uid) {
			%>
			<tr>
				<td><%=gatedevice.getId() %></td>
				<td><%=gatedevice.getGid()%></td>
				<td><%=gatedevice.getDid()%></td>
				<td><%=gatedevice.getClientdevicename() %></td>
				<td><%=gatedevice.getClientdeviceid() %></td>
				<td><%=gatedevice.getAddtime() %></td>
				<td>
				<a href="GateDeviceServlet?action=update&id=<%=gatedevice.getId()%>">
					<i class="glyphicon glyphicon-pencil"></i>
				</a>
				<a href="javascript:void(0)" onclick="javascript:if(confirm('是否确认要删除？')) location.href='GateDeviceServlet?action=delete&id=<%=gatedevice.getId()%>'">
					<i class="glyphicon glyphicon-trash"></i>
				</a>
				</td>
			</tr>
			<%}
	             }
	             ud.closedb();
	         } %>

		</tbody>
	</table>

	<!-- 下面是控制分页控件，必须要是ul元素才行 -->
	<ul id='bp-element'></ul>
<%
String msg="";
if(request.getAttribute("msg")!=null){
	msg=request.getAttribute("msg").toString();
}
if(msg.length()>0){
//显示提示信息
%>
  
 <!-- Bootstrap 警告（Alerts） --> 
 <div class="alert alert-info" id="statusbar" data-role="alert"><%=msg %></div>
<% 
}
%>
	

</div>

<%
//获取当前页面和总页数
String currentPage=request.getAttribute("currentPage").toString();
String total=request.getAttribute("total").toString();

%>
<script type="text/javascript">
$(function(){

        var element = $('#bp-element');// bp-element上方刚定义的ul元素
        options = {
            bootstrapMajorVersion:3, //对应的bootstrap版本
            currentPage: <%=currentPage%>, //当前请求页面，获取从后台传过来的值
            numberOfPages: 5, //控件显示的页码数。
            totalPages:<%=total%>, //总页数，获取从后台传过来的值
            shouldShowPage:true,//某操作按钮是否显示
            itemTexts: function (type, page, current) {
//控制页眉（操作按钮）的显示样式，将操作按钮上的英文显示为中文
                switch (type) {
                    case "first":
                        return "首页";
                    case "prev":
                        return "上一页";
                    case "next":
                        return "下一页";
                    case "last":
                        return "末页";
                    case "page":
                        return page;
                }
            },
            //点击事件
            onPageClicked: function (event, originalEvent, type, page) {
            	
                location.href = "DeviceTypeServlet?page=" + page +"&fieldname=<%=fieldname%>&keyword=<%=keyword%>";
            }
        };
        element.bootstrapPaginator(options);

});
</script>

<jsp:include page="foot.jsp"></jsp:include>
