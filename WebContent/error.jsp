<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<jsp:include page="head.jsp"></jsp:include>
<style>
.py-5 {
	padding-top: 3rem !important;
	padding-bottom: 3rem !important;
}

@media screen and (min-width: 1200px) {
	.display-1 {
		font-size: 5rem;
	}
}

.display-1 {
	font-size: calc(1.625rem + 4.5vw);
	font-weight: 300;
	line-height: 1.2;
}
</style>
<div class="text-center py-5">
	<h1 class="display-1">Error</h1>
	<h2>抱歉，出现了意外的错误</h2>
</div>
<jsp:include page="foot.jsp"></jsp:include>