<%@ page language="java" import="java.util.*,iot.bean.*"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<jsp:include page="head.jsp"></jsp:include>
<!-- 后面整合到head.jsp -->
<link href="js/bootstrap-switch/css/bootstrap3/bootstrap-switch.min.css"
	rel="stylesheet">
<script src="js/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<script src="js/jquery.cookie.js"></script>
<script src="js/radialIndicator-master/radialIndicator.js"></script>
<style type="text/css">
#led {
	width: 150px;
	height: 150px;
	margin: 5px auto;
	border-radius: 50%;
}

.r { /*绘制红色圆球 */
	background: radial-gradient(#ff0000, #000000); /*径向渐变，红色到黑色的渐变 */
	box-shadow: 0 0 50px #ff0000; /*添加阴影*/
}

.g {
	background: radial-gradient(#00ff00, #000000);
	box-shadow: 0 0 50px #00ff00;
}

.b {
	background: radial-gradient(#0000ff, #000000);
	box-shadow: 0 0 50px #0000ff;
}

.off {
	background: radial-gradient(#fff, #cecece);
}
</style>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/paho-mqtt/1.0.1/mqttws31.min.js"
	type="text/javascript"></script>
<jsp:include page="realScripts.jsp"></jsp:include>
<div class="container">
	<ol class="breadcrumb">
		<li><a href="index.jsp">Home</a></li>
		<li class="active">实时监控</li>
	</ol>
	
	<div class="btn-toolbar">
		<a class="btn btn-primary" href="GateDeviceServlet">
			<i class="glyphicon glyphicon-th-list"></i> 监控设备管理
		</a>
	</div>
	<br/>
	<!-- 执行器面板实例 -->
	<div class="col-md-6">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title text-center">3色LED灯监控</h3>
			</div>
			<div class="panel-body">
				<div class="col-md-8 text-center">
					<div id="led" class="off"></div>
				</div>
				<div class="col-md-4 text-center">
					<div class="btn-group-vertical">
						<div class="switch" data-on="danger" data-off="primary">

							<input id="ledswitch" type="checkbox" onchange="onChg(this)"/>
						</div>
						<button type="button" id="ledr" class="btn btn-danger">红色</button>
						<button type="button" id="ledg" class="btn btn-success">绿色</button>
						<button type="button" id="ledb" class="btn btn-primary">蓝色</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
	
		var flag = false;
		function onChg(obj) {
			if (!obj.checked) {
				if (flag)
					return;
				var dataJson = {
					power : "off"
				};
				mqttsend("RGBLED", dataJson);
				flag = true;
			} else {
				console.log("请选择颜色！");
				flag = false;
			}
		}

		function DHT11_SW(obj) {
			if (obj.checked)
				refreshtimer = window.setInterval(refresh, 1500);
			else {
				window.clearInterval(refreshtimer);
				refreshtimer = 0;
			}
		}
	</script>
	<!-- 传感器面板实例 -->
	<div class="col-md-6">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title text-center">温湿度监控</h3>
			</div>
			<div class="panel-body">
				<div class="col-md-6 text-center">
					<div id="temp"></div>
					<h5>温度</h5>
				</div>
				<div class="col-md-6 text-center">
					<div id="humi"></div>
					<h5>湿度</h5>
				</div>
				<div class="col-md-offset-4 col-lg-offset-4col-xl-offset-4"
					style="margin: 0 auto; margin-left: 30%; display: block; width: 200px;position:relative">
					<div class="switch" data-on="danger" data-off="primary">
						实时数据开关 <input id="dataswitch" type="checkbox" onchange="DHT11_SW(this)"/>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="alert alert-info" id="statusbar" data-role="alert"></div>
</div>
<jsp:include page="foot.jsp"></jsp:include>