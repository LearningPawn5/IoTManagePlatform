<%@ page language="java" import="java.util.*,iot.bean.*"
	pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>	

<jsp:include page="head.jsp"></jsp:include>

<div class="container">
	<ol class="breadcrumb">
		<li><a href="index.jsp">Home</a></li>
		<li><a href="DeviceTypeServlet">设备类型管理</a></li>
		<li class="active">添加设备类型</li>
	</ol>

<!-- Bootstrap 面板（Panels）-->
	<div class="panel panel-default">
		<div class="panel-heading">添加设备类型信息</div>
		<div class="panel-body">

			<!-- form-horizontal：水平表单，元素水平排列  ?action=add 带参数时 必须是post-->
			<form class="form-horizontal" action="DeviceTypeServlet?action=add" method="post" data-role="form">
				<div class="form-group">
				   <!--  col-md-4：表示12列占用4列，可以控制宽度（bootstrap网格系统） -->
					<label class="col-md-4 control-label">设备类型代码：</label>
					<div class="col-md-4">
						<input class="form-control" name="devicetypecode" id="devicetypecode" placeholder="请输入设备类型代码" type="text" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">设备类型名称：</label>
					<div class="col-md-4">
						<input class="form-control" name="devicetypename" id="devicetypename" placeholder="请输入设备类型名称" type="text" required>						
					</div>
				</div>
				<div class="form-group">
				</div>
				<div class="form-group">
				    <label class="col-md-4 control-label"></label>
				    <div class="col-md-4">
				         <button type="submit" class="btn btn-default">保存</button>
				         <a class="btn btn-default" href="DeviceTypeServlet">返回</a>
				    </div>
				</div>
			</form>
		</div>
	</div>

</div>
<jsp:include page="foot.jsp"></jsp:include>
