<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
<%
	if (session.getAttribute("loginusername") == null) {
%>
<head>
<script type="text/javascript">
	alert("您还没有登录，请登录...");
	location.href = "login.jsp";
</script>
</head>
<%
	return;
	}
%>
<head>
<meta charset="utf-8">
<!-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/> -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>物联网监控平台</title>
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link href="js/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link
	href="js/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"
	rel="stylesheet">

<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/bootstrap/js/bootstrap.js"></script>
<!-- 分页插件 -->
<script src="js/bootstrap-paginator.min.js"></script>
<script src="js/echarts.common.min.js"></script>
<script src="js/moment-with-locales.min.js"></script>
<script
	src="js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="js/jquery.cookie.js"></script>
<script src="js/mqttws31.min.js"></script>
<style>
.item {
	height: 270px;
	position: relative;
	margin: 0 auto
}

.item img {
	position: absolute;
	top: -100%;
	margin-top: -5%
}

@media screen and (max-width: 768px) {
	.item {
		height: auto;
		position:static;
	}
	.item img {
	position: static;
	}
}
</style>
</head>
<body>

	<nav class="navbar navbar-default navbar-static-top"
		data-role="navigation">
		<div id="carousel-ad" class="carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
				<li data-target="#carousel-ad" data-slide-to="0" class="active"></li>
				<li data-target="#carousel-ad" data-slide-to="1"></li>
				<li data-target="#carousel-ad" data-slide-to="2"></li>
				<li data-target="#carousel-ad" data-slide-to="3"></li>
			</ol>
			<div class="carousel-inner" data-role="listbox">			
				<div class="item active">
					<img class="img-responsive" src="images/325185.jpg">
					<div class="carousel-caption">
						<h1>智慧城市</h1>
						<p>物联网，开启万物互联新时代</p>
					</div>
				</div>
				<div class="item">
					<img class="img-responsive" src="images/1148820.jpg">
					<div class="carousel-caption">
						<h1>物联网监控平台</h1>
						<p>为物联网相关设备提供网上应用交互</p>
						<p><a class="btn btn-lg btn-primary" href="DeviceServlet" data-role="button">立即体验</a></p>
					</div>
				</div>
				<div class="item">
					<img class="img-responsive" src="images/356043.jpg">
					<div class="carousel-caption">
						<h3>从零开始的物联网项目</h3>
						<p>简单、方便、快捷，部署一个物联网项目可能只需要简单几步</p>
						<p><a class="btn btn-lg btn-primary" href="ProjectServlet" data-role="button">点我开始</a></p>
					</div>
				</div>
				<div class="item">
					<img class="img-responsive" src="images/107728.png">
					<div class="carousel-caption">
						<h2>实时获取，及时快捷</h2>
						<p>部署项目，查看传感器的实时状态信息</p>
						<p><a class="btn btn-lg btn-primary" href="real.jsp" data-role="button">点我查看</a></p>
					</div>
				</div>
			</div>
			<a class="left carousel-control" href="#carousel-ad"
				data-role="button" data-slide="prev"> <span
				class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				<span class="sr-only">前一张</span>
			</a> <a class="right carousel-control" href="#carousel-ad"
				data-role="button" data-slide="next"> <span
				class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				<span class="sr-only">后一张</span>
			</a>
		</div>

		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.jsp">物联网监控平台</a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<ul id="navbar-btn" class="nav navbar-nav">
					<li><a href="UserServlet">用户管理</a></li>
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown"> 设备管理<b class="caret"></b>
					</a>
						<ul class="dropdown-menu">
							<li><a href="DeviceTypeServlet">设备类型管理</a></li>
							<li><a href="DeviceServlet">设备型号管理</a></li>
						</ul></li>
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown"> 监控设置<b class="caret"></b>
					</a>
						<ul class="dropdown-menu">
							<li><a href="ProjectServlet">监控项目管理</a></li>
							<li><a href="GateServlet">监控网关管理</a></li>
							<li><a href="GateDeviceServlet">监控设备管理</a></li>
						</ul></li>
					<li><a href="real.jsp">实时监控</a></li>
					<li><a href="HistoryDataServlet">历史数据</a></li>
				</ul>
				<form class="navbar-form navbar-right">
					<label id="currentTime"></label>
					<script type="text/javascript">
					setInterval(function() {
						document.getElementById("currentTime").innerHTML = 
							new Date().toLocaleString('zh-CN', {hour12 : false});
					}, 1000);
						$(document).ready(function() {
							// each 是 为每一个匹配的元素 执行定义的方法
							$("#navbar-btn").find("li").each(function() {
								var a = $(this).find("a:first")[0];
								// location.pathname 获取 当前浏览器上的url 地址
								if ($(a).attr("href") == location.pathname.split("/")[2]) {
									$(this).addClass("active");
								} else {
									$(this).removeClass("active");
								}
							});
						});
					</script>
					<br /> 
						<label><%=session.getAttribute("loginusername")%>, <%=session.getAttribute("loginusertype")%>, 
						<a href="UserServlet?action=logout">
							<span class="glyphicon glyphicon-log-out"></span>&nbsp;退出
						</a></label>
				</form>
			</div>
		</div>
	</nav>
</body>
</html>