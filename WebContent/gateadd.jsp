<%@page import="iot.dao.UtilsDao"%>
<%@ page language="java" import="java.util.*,iot.bean.*"
	pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String UserType = session.getAttribute("loginusertype").toString();
String UserName = session.getAttribute("loginusername").toString();
%>	

<jsp:include page="head.jsp"></jsp:include>

<div class="container">
	<ol class="breadcrumb">
		<li><a href="index.jsp">Home</a></li>
		<li><a href="GateServlet">网关管理</a></li>
		<li class="active">添加网关信息</li>
	</ol>

<!-- Bootstrap 面板（Panels）-->
	<div class="panel panel-default">
		<div class="panel-heading">添加网关信息</div>
		<div class="panel-body">

			<!-- form-horizontal：水平表单，元素水平排列  ?action=add 带参数时 必须是post-->
			<form class="form-horizontal" action="GateServlet?action=add" method="post" data-role="form">
				<div class="form-group">
				   <!--  col-md-4：表示12列占用4列，可以控制宽度（bootstrap网格系统） -->
					<label class="col-md-4 control-label">网关代码：</label>
					<div class="col-md-4">
						<input class="form-control" name="gateid" id="gateid" placeholder="请输入网关代码，通讯端使用，必须唯一，可以采用UUID" type="text" required>
					</div>
				</div>
				<div class="form-group">
				   <!--  col-md-4：表示12列占用4列，可以控制宽度（bootstrap网格系统） -->
					<label class="col-md-4 control-label">项目ID：</label>
					<div class="col-md-4">
						
							<%
								UtilsDao ud = new UtilsDao();
								Long uid = ud.getUidByUserName(UserName);
								Map<Long, String> values = new HashMap<Long, String>();
								if(UserType.equals("管理员")) {
									values = ud.relProjectAll();
								} else {
									values = ud.relProjectByUid(uid);
								}
								ud.closedb();
								boolean flag = (values.size()>0);
								if (flag)
									out.println("<select id='pid' name='pid' class='form-control' title='请选择您的项目ID'>");
								for (Map.Entry<Long, String> entry : values.entrySet()) {
									out.println("<option value='" + entry.getKey() + "'>" + entry.getValue() + "</option>");
								}
								if (!flag)
									out.println("<a class='btn btn-default' href='projectadd.jsp'> <i			class='glyphicon glyphicon-plus'></i> 您还没有创建项目，点击添加项目		</a>");
								else
									out.println("</select>");
							%>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">网关名称：</label>
					<div class="col-md-4">
						<input class="form-control" name="gatename" id="gatename" placeholder="请输入网关名称" type="text" required>
					</div>
				</div>
				<div class="form-group">
				</div>
				<div class="form-group">
				    <label class="col-md-4 control-label"></label>
				    <div class="col-md-4">
				         <button type="submit" class="btn btn-default" <%=flag?"":"disabled='disabled'" %>>保存</button>
				         <a class="btn btn-default" href="GateServlet">返回</a>
				    </div>
				</div>
			</form>
		</div>
	</div>

</div>
<jsp:include page="foot.jsp"></jsp:include>
