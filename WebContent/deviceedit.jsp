<%@page import="iot.dao.UtilsDao"%>
<%@ page language="java" import="java.util.*,iot.bean.*"
	pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<jsp:include page="head.jsp"></jsp:include>
<link href="js/fileinput/css/fileinput.min.css" rel="stylesheet">

<script src="js/fileinput/js/fileinput.js"></script>
<script src="js/fileinput/js/locales/zh.js"></script>
<div class="container">
	<ol class="breadcrumb">
		<li><a href="index.jsp">Home</a></li>
		<li><a href="DeviceServlet">设备型号管理</a></li>
		<li class="active">编辑设备</li>
	</ol>

<!-- Bootstrap 面板（Panels）-->
	<div class="panel panel-default">
		<div class="panel-heading">编辑设备信息</div>
		<div class="panel-body">
		<jsp:useBean id="device" class="iot.bean.Tdevice" scope="request">
		       <jsp:setProperty name="device" property="*" />
		</jsp:useBean>

			<!-- form-horizontal：水平表单，元素水平排列  ?action=add 带参数时 必须是post-->
			<form class="form-horizontal" action="DeviceServlet?action=updateSave" method="post" data-role="form" enctype="multipart/form-data" >
				<div class="form-group">
					<label class="col-md-4 control-label">设备类型ID：</label>
					<div class="col-md-4">
					    <select id="dtid" name="dtid" class="form-control" title="请选择设备类型">
							<%
								UtilsDao ud = new UtilsDao();
								Map<Long, String> values = ud.relDeviceTypeAll();
								ud.closedb();
								for (Map.Entry<Long, String> entry : values.entrySet()) {
									out.println("<option value='" + entry.getKey() + "' "+(device.getDtid()==entry.getKey()?"selected":"")+">" + entry.getValue() + "</option>");
								}
							%>
						</select>
						<input type="hidden" name="id"  value="<%=device.getId() %>"  id="id" >
				    </div>
				</div>
				<div class="form-group">
				   <!--  col-md-4：表示12列占用4列，可以控制宽度 -->
					<label class="col-md-4 control-label">设备型号名称：</label>
					<div class="col-md-4">
						<input class="form-control" name="devicename" id="devicename" value="<%=device.getDevicename() %>" placeholder="设备型号的名称描述" type="text" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">设备型号：</label>
					<div class="col-md-4">
						<input class="form-control" name="devicecode" id="devicecode"
							value="<%=device.getDevicecode() %>" placeholder="终端通讯备用，必须唯一"
							type="text">
					</div>
				</div>
				<div class="form-group">
					<label for="devicephoto" class="col-md-4 control-label">设备照片：</label>
					<div class="col-md-4">
						<input class="form-control" name="devicephoto" id="devicephoto" type="file">
					</div>
				</div>				
				<script type="text/javascript">
					$("#devicephoto").fileinput({
						language : 'zh',
						autoReplace : true,
						showUpload: false,
						maxFileCount : 1,
						allowedFileExtensions : [ "jpg", "png", "gif" ],
						browseClass : "btn btn-primary", //按钮样式 
						previewFileIcon : "<i class='glyphicon glyphicon-ban-circle'></i>"
					}).on("fileuploaded", function(e, data) {
						var res = data.response;
						alert(res.success);
					});
				</script><%-- 
				<div class="form-group">
					<label class="col-md-4 control-label">设备照片：</label>
					<div class="col-md-4">
						<input class="form-control" name="devicephoto" id="devicephoto"
							value="<%=device.getDevicephoto() %>" type="file">
					</div>
				</div> --%>
				<div class="form-group">
				    <label class="col-md-4 control-label"></label>
				    <div class="col-md-4">
				         <button type="submit" class="btn btn-default">保存</button>
				         <a class="btn btn-default" href="DeviceServlet">返回</a>
				    </div>
				</div>
			</form>
		</div>
	</div>

</div>


<jsp:include page="foot.jsp"></jsp:include>
