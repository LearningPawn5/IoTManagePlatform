<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<!-- <meta http-equiv="X-UA-Compatible" content="IE=edge">-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>物联网监控平台</title>
<!-- 1、引入 Bootstrap -->
<link href="js/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!-- 2、Bootstrap 的 JavaScript 插件需要引入 jQuery -->
<script src="js/jquery-1.11.3.min.js"></script>
<!-- 3、包括所有已编译的插件 -->
<script src="js/bootstrap/js/bootstrap.min.js"></script>
<style type="text/css">
body {
	padding-top: 40px;
	padding-bottom: 40px;
	background-color: #eee;
}

.form-signin {
	max-width: 330px;
	padding: 15px;
	margin: 0 auto;
}

.form-signin .form-signin-heading, .form-signin .checkbox {
	margin-bottom: 10px;
}

.form-signin .checkbox {
	font-weight: normal;
}

.form-signin .form-control {
	position: relative;
	height: auto;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	padding: 10px;
	font-size: 16px;
}

.form-signin .form-control:focus {
	z-index: 2;
}

.form-signin input[type="text"] {
	margin-bottom: -1px;
	border-bottom-right-radius: 0;
	border-bottom-left-radius: 0;
}

.form-signin input[type="password"] {
	margin-bottom: 10px;
	border-top-left-radius: 0;
	border-top-right-radius: 0;
}
</style>
<script type="text/javascript">
	
</script>
</head>
<body>

	<div class="container">
		<form class="form-signin" action="UserServlet?action=login"
			method="post">
			<h2 class="form-signin-heading">物联网监控平台登录</h2>
			<label for="username" class="sr-only">用户名：</label> <input type="text"
				id="username" name="username" class="form-control"
				placeholder="请输入用户名" required autofocus> <label
				for="password" class="sr-only">密 码：</label> <input type="password"
				id="password" name="password" class="form-control"
				placeholder="请输入密码" required>
			<div class="checkbox">
				<label> <input type="checkbox" id="remember" name="remember"
					value="1"> 记住我
				</label>
			</div>
			<button class="btn btn-lg btn-primary btn-block" id="login"
				type="submit">登录</button>
				<%
			String msg = "";
			if (request.getAttribute("msg") != null) {
				msg = request.getAttribute("msg").toString();
			}
			if (msg.length() > 0) {
				//显示提示信息
		%>
			<br>
			<div class="alert alert-warning" id="statusbar" data-role="alert"><%=msg %></div>
			<%
}
%>
		</form>
		
	</div>
</body>
</html>
