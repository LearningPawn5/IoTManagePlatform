﻿/*
 * Glue-Panels, a javascript can initial or change some special panels.
 *
 * Copyright (c) 2021, LearningPawn5
 * All rights reserved.
 *
 * Need
 * jQuery.js, etc.
 */
function initChart(myChart, descrpit, code) {
	$(function(){
		option = {
				title: { //标题组件
					text: descrpit + '折线图'
				},
				tooltip: { //提示框组件
					trigger: 'axis'
				},
				legend: { //图例组件
					data: []
				},
				grid : { //直角坐标系内绘图网格
					left : '3%',
					right : '4%',
					bottom : '3%',
					containLabel : true
				},
				toolbox : { //工具栏
					feature : {
						saveAsImage : {}
					}
				},
				xAxis : { //直角坐标系 grid 中的 x 轴
					type : 'category',
					boundaryGap : false,
					data: []
				},
				yAxis : { //直角坐标系 grid 中的 y 轴
					type : 'value'
				}			
		};
		myChart.setOption(option);
		$('#'+code+'-chart').css('display', 'none');
	});
}

function initBtnFuncs(myChart, code){
	$.ajax({
		type : "post",
		url : "HistoryDataServlet?action=query&starttime="
				+ $("#"+code+"-dtp1").find("input").val()
				+ "&endtime="
				+ $("#"+code+"-dtp2").find("input").val(),
		success : function(data) {
			console.log(code+" log: query OK. And data: " + data);
			jsondata = JSON.parse(data);
			times = [];
			humis = [];
			temps = [];
			//参数设置
			option = {
				title : { //标题组件
					text : '温湿度监控' + '折线图'
				},
				tooltip : { //提示框组件
					trigger : 'axis'
				},
				legend : { //图例组件
					data : [ '温度', '湿度' ]
				},
				grid : { //直角坐标系内绘图网格
					left : '3%',
					right : '4%',
					bottom : '3%',
					containLabel : true
				},
				toolbox : { //工具栏
					feature : {
						saveAsImage : {}
					}
				},
				xAxis : { //直角坐标系 grid 中的 x 轴
					type : 'category',
					boundaryGap : false,
					data : jsondata.time
				},
				yAxis : { //直角坐标系 grid 中的 y 轴
					type : 'value'
				},
				series : [ //系列列表
				{
					name : '温度',
					type : 'line',
					data : jsondata.temp
				}, {
					name : '湿度',
					type : 'line',
					data : jsondata.humi
				}, ]
			};
			$('#container').css('display', 'block');
			myChart.setOption(option); //参数设置方法
		},
		error : function(result) {
			$("#statusbar")
					.attr("class", "alert alert-warning");
			$("#statusbar").text("连接数据库服务器失败！ ");
		}
	});
}