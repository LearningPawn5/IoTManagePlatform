package iot.dao;

import iot.bean.*;
import iot.utils.ConnDb;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class GateDao {
	private Connection cn = null;
	private PreparedStatement ps = null;
	private ResultSet rs = null;
	public static final int PAGE_LENGTH = 5;
	public void closedb() {
		try {
			if (rs != null) {
				rs.close();
			}
			if (ps != null) {
				ps.close();
			}
			if (cn != null) {
				cn.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ArrayList<Tgate> query(String conStr) {
		ArrayList<Tgate> retlist = new ArrayList<Tgate>();
		try {
			cn = new ConnDb().getcon();
			String sqlstr = "select  * from tgate  ";
			if (conStr != "") {
				sqlstr = "select  * from tgate where " + conStr + " ";
			}
			System.out.println(sqlstr);
			ps = cn.prepareStatement(sqlstr);

			rs = ps.executeQuery();
			while (rs.next()) {
				Tgate temp = new Tgate();
				temp.setId(rs.getLong("id"));
				temp.setPid(rs.getLong("pid"));
				temp.setGateid(rs.getString("gateid"));
				temp.setGatename(rs.getString("gatename"));
				temp.setGateenabled(rs.getInt("gateenabled"));
				temp.setRemark(rs.getString("remark"));
				temp.setAddtime(rs.getTimestamp("addtime"));
				retlist.add(temp);
			}

		} catch (Exception e) {
			
		
			retlist = null;
			e.printStackTrace();
		} finally {
			this.closedb();
		}

		return retlist;
	}

	public ArrayList<Tgate> queryPage(String conStr, int page) {
		ArrayList<Tgate> retlist = new ArrayList<Tgate>();
		try {
			cn = new ConnDb().getcon();

			int begin = (page - 1) * PAGE_LENGTH;

String sqllimit = "  order by id desc limit " + begin + ","+ PAGE_LENGTH;

			String sqlstr = "select  * from tgate " + sqllimit;
			if (conStr != "") {
				sqlstr = "select  * from tgate where " + conStr + sqllimit;
			}
			 System.out.println(sqlstr);
			ps = cn.prepareStatement(sqlstr);

			rs = ps.executeQuery();
			while (rs.next()) {
				Tgate temp = new Tgate();
				temp.setId(rs.getLong("id"));
				temp.setPid(rs.getLong("pid"));
				temp.setGateid(rs.getString("gateid"));
				temp.setGatename(rs.getString("gatename"));
				temp.setGateenabled(rs.getInt("gateenabled"));
				temp.setRemark(rs.getString("remark"));
				temp.setAddtime(rs.getTimestamp("addtime"));
				retlist.add(temp);

			}

		} catch (Exception e) {
			retlist = null;
			e.printStackTrace();
		} finally {
			this.closedb();
		}

		return retlist;
	}

	public int count(String conStr) {
		int n = 0;
		try {
			cn = new ConnDb().getcon();
			String sqlstr = "select  count(*) from tgate ";
			if (conStr != "") {
				sqlstr = "select  count(*) from tgate where " + conStr;
			}
			// System.out.println(sqlstr);
			ps = cn.prepareStatement(sqlstr);

			rs = ps.executeQuery();
			if (rs.next()) {

				n = rs.getInt(1);

			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.closedb();
		}
		return n;
	}

	public boolean add(Tgate gate) {
		boolean addFlag = false;
		try {
			cn = new ConnDb().getcon();
			String sqlstr = "insert into tgate(gateid,gatename,addtime,pid) values	(?,?,?,?) ";
			ps = cn.prepareStatement(sqlstr);
			ps.setString(1, gate.getGateid());
			ps.setString(2, gate.getGatename());
			ps.setTimestamp(3, gate.getAddtime());
			ps.setLong(4, gate.getPid());
		
			int rows = ps.executeUpdate();
			if (rows > 0) {
				addFlag = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.closedb();
		}
		return addFlag;
	}

	public boolean update(Tgate gate) {
		boolean altFlag = false;
		try {
			cn = new ConnDb().getcon();
			String sqlstr = "update tgate set gateid=?,gatename=?,addtime=?,pid=? where id=? ";

			ps = cn.prepareStatement(sqlstr);
			ps.setString(1, gate.getGateid());
			ps.setString(2, gate.getGatename());
			ps.setTimestamp(3, gate.getAddtime());
			ps.setLong(4, gate.getPid());
			ps.setLong(5, gate.getId());

			int rows = ps.executeUpdate();
			if (rows > 0) {
				altFlag = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.closedb();
		}
		return altFlag;
	}

	public boolean delete(String number) {
		boolean delFlag = false;
		try {
			cn = new ConnDb().getcon();

			String sqlstr = "delete from  tgate  where id=" + number + "  ";
			ps = cn.prepareStatement(sqlstr);

			int rows = ps.executeUpdate();
			if (rows > 0) {
				delFlag = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.closedb();
		}
		return delFlag;
	}


}
