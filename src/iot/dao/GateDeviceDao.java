package iot.dao;

import iot.utils.ConnDb;
import iot.bean.Tgatedevice;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class GateDeviceDao {
	private Connection cn = null;
	private PreparedStatement ps = null;
	private ResultSet rs = null;
	public static final int PAGE_LENGTH = 5;

	public void closedb() {
		try {
			if (rs != null) {
				rs.close();
			}
			if (ps != null) {
				ps.close();
			}
			if (cn != null) {
				cn.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ArrayList<Tgatedevice> query(String conStr) {
		ArrayList<Tgatedevice> retlist = new ArrayList<Tgatedevice>();
		try {
			cn = new ConnDb().getcon();
			String sqlstr = "select  * from tgatedevice  ";
			if (conStr != "") {
				sqlstr = "select  * from tgatedevice where " + conStr + " ";
			}
			System.out.println(sqlstr);
			ps = cn.prepareStatement(sqlstr);

			rs = ps.executeQuery();
			while (rs.next()) {
				Tgatedevice temp = new Tgatedevice();
				temp.setId(rs.getLong("id"));
				temp.setGid(rs.getLong("gid"));
				temp.setDid(rs.getLong("did"));
				temp.setClientdevicename(rs.getString("clientdevicename"));
				temp.setClientdeviceid(rs.getString("clientdeviceid"));
				temp.setClientdeviceenabled(rs.getString("clientdeviceenabled"));
				temp.setClientdeviceconfig(rs.getString("clientdeviceconfig"));
				temp.setClientdevicestate(rs.getString("clientdevicestate"));
				temp.setRemark(rs.getString("remark"));
				temp.setAddtime(rs.getTimestamp("addtime"));
				retlist.add(temp);
			}

		} catch (Exception e) {

			retlist = null;
			e.printStackTrace();
		} finally {
			this.closedb();
		}

		return retlist;
	}

	public ArrayList<Tgatedevice> queryPage(String conStr, int page) {
		ArrayList<Tgatedevice> retlist = new ArrayList<Tgatedevice>();
		try {
			cn = new ConnDb().getcon();

			int begin = (page - 1) * PAGE_LENGTH;

			String sqllimit = "  order by id desc limit " + begin + "," + PAGE_LENGTH;

			String sqlstr = "select  * from tgatedevice " + sqllimit;
			if (conStr != "") {
				sqlstr = "select  * from tgatedevice where " + conStr + sqllimit;
			}
			System.out.println(sqlstr);
			ps = cn.prepareStatement(sqlstr);

			rs = ps.executeQuery();
			while (rs.next()) {
				Tgatedevice temp = new Tgatedevice();
				temp.setId(rs.getLong("id"));
				temp.setGid(rs.getLong("gid"));
				temp.setDid(rs.getLong("did"));
				temp.setClientdevicename(rs.getString("clientdevicename"));
				temp.setClientdeviceid(rs.getString("clientdeviceid"));
				temp.setClientdeviceenabled(rs.getString("clientdeviceenabled"));
				temp.setClientdeviceconfig(rs.getString("clientdeviceconfig"));
				temp.setClientdevicestate(rs.getString("clientdevicestate"));
				temp.setRemark(rs.getString("remark"));
				temp.setAddtime(rs.getTimestamp("addtime"));
				retlist.add(temp);
			}

		} catch (Exception e) {
			retlist = null;
			e.printStackTrace();
		} finally {
			this.closedb();
		}

		return retlist;
	}

	public int count(String conStr) {
		int n = 0;
		try {
			cn = new ConnDb().getcon();
			String sqlstr = "select count(*) from tgatedevice ";
			if (conStr != "") {
				sqlstr = "select count(*) from tgatedevice where " + conStr;
			}
			// System.out.println(sqlstr);
			ps = cn.prepareStatement(sqlstr);

			rs = ps.executeQuery();
			if (rs.next()) {

				n = rs.getInt(1);

			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.closedb();
		}
		return n;
	}

	public boolean add(Tgatedevice gatedevice) {
		boolean addFlag = false;
		try {
			cn = new ConnDb().getcon();
			String sqlstr = "insert into tgatedevice(gid,did,clientdevicename,clientdeviceid,addtime) values(?,?,?,?,?) ";
			ps = cn.prepareStatement(sqlstr);
			ps.setLong(1, gatedevice.getGid());
			ps.setLong(2, gatedevice.getDid());
			ps.setString(3, gatedevice.getClientdevicename());
			ps.setString(4, gatedevice.getClientdeviceid());
			ps.setTimestamp(5, gatedevice.getAddtime());
			int rows = ps.executeUpdate();
			if (rows > 0) {
				addFlag = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.closedb();
		}
		return addFlag;
	}

	public boolean update(Tgatedevice gatedevice) {
		boolean altFlag = false;
		try {
			cn = new ConnDb().getcon();
			String sqlstr = "update tgatedevice set gid=?,did=?,clientdevicename=?,clientdeviceid=?,addtime=? where id=? ";

			ps = cn.prepareStatement(sqlstr);
			ps.setLong(1, gatedevice.getGid());
			ps.setLong(2, gatedevice.getDid());
			ps.setString(3, gatedevice.getClientdevicename());
			ps.setString(4, gatedevice.getClientdeviceid());
			ps.setTimestamp(5, gatedevice.getAddtime());
			ps.setLong(6, gatedevice.getId());

			int rows = ps.executeUpdate();
			if (rows > 0) {
				altFlag = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.closedb();
		}
		return altFlag;
	}

	public boolean delete(String number) {
		boolean delFlag = false;
		try {
			cn = new ConnDb().getcon();

			String sqlstr = "delete from tgatedevice where id=" + number + "  ";
			ps = cn.prepareStatement(sqlstr);

			int rows = ps.executeUpdate();
			if (rows > 0) {
				delFlag = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.closedb();
		}
		return delFlag;
	}

}