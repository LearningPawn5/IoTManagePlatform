package iot.dao;

import iot.utils.ConnDb;
import iot.bean.Tdevicetype;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class DeviceTypeDao {
	private Connection cn = null;
	private PreparedStatement ps = null;
	private ResultSet rs = null;
	public static final int PAGE_LENGTH = 5;
	public void closedb() {
		try {
			if (rs != null) {
				rs.close();
			}
			if (ps != null) {
				ps.close();
			}
			if (cn != null) {
				cn.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ArrayList<Tdevicetype> query(String conStr) {
		ArrayList<Tdevicetype> retlist = new ArrayList<Tdevicetype>();
		try {
			cn = new ConnDb().getcon();
			String sqlstr = "select  * from tdevicetype  ";
			if (conStr != "") {
				sqlstr = "select  * from tdevicetype where " + conStr + " ";
			}
			System.out.println(sqlstr);
			ps = cn.prepareStatement(sqlstr);

			rs = ps.executeQuery();
			while (rs.next()) {
				Tdevicetype temp = new Tdevicetype();
				temp.setId(rs.getLong("id"));
				temp.setDevicetypename(rs.getString("devicetypename"));
				temp.setDevicetypecode(rs.getString("devicetypecode"));
				temp.setDevicetypeconfig(rs.getString("devicetypeconfig"));
				temp.setRemark(rs.getString("remark"));
				temp.setAddtime(rs.getTimestamp("addtime"));
				retlist.add(temp);
			}

		} catch (Exception e) {
			
		
			retlist = null;
			e.printStackTrace();
		} finally {
			this.closedb();
		}

		return retlist;
	}
	public ArrayList<Tdevicetype> queryPage(String conStr, int page) {
		ArrayList<Tdevicetype> retlist = new ArrayList<Tdevicetype>();
		try {
			cn = new ConnDb().getcon();

			int begin = (page - 1) * PAGE_LENGTH;

String sqllimit = "  order by id desc limit " + begin + ","+ PAGE_LENGTH;

			String sqlstr = "select  * from tdevicetype " + sqllimit;
			if (conStr != "") {
				sqlstr = "select  * from tdevicetype where " + conStr + sqllimit;
			}
			 System.out.println(sqlstr);
			ps = cn.prepareStatement(sqlstr);

			rs = ps.executeQuery();
			while (rs.next()) {
				Tdevicetype temp = new Tdevicetype();
				temp.setId(rs.getLong("id"));
				temp.setDevicetypename(rs.getString("devicetypename"));
				temp.setDevicetypecode(rs.getString("devicetypecode"));
				temp.setDevicetypeconfig(rs.getString("devicetypeconfig"));
				temp.setRemark(rs.getString("remark"));
				temp.setAddtime(rs.getTimestamp("addtime"));
				retlist.add(temp);
			}

		} catch (Exception e) {
			retlist = null;
			e.printStackTrace();
		} finally {
			this.closedb();
		}

		return retlist;
	}

	public int count(String conStr) {
		int n = 0;
		try {
			cn = new ConnDb().getcon();
			String sqlstr = "select  count(*) from tdevicetype ";
			if (conStr != "") {
				sqlstr = "select  count(*) from tdevicetype where " + conStr;
			}
			// System.out.println(sqlstr);
			ps = cn.prepareStatement(sqlstr);

			rs = ps.executeQuery();
			if (rs.next()) {

				n = rs.getInt(1);

			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.closedb();
		}
		return n;
	}

	public boolean add(Tdevicetype devicetype) {
		boolean addFlag = false;
		try {
			cn = new ConnDb().getcon();
			String sqlstr = "insert into tdevicetype(devicetypecode,devicetypename,addtime) values	(?,?,?) ";
			ps = cn.prepareStatement(sqlstr);
			ps.setString(1, devicetype.getDevicetypecode());
			System.out.println(devicetype.getDevicetypecode());
			ps.setString(2, devicetype.getDevicetypename());
			System.out.println(devicetype.getDevicetypename());
			ps.setTimestamp(3,devicetype.getAddtime());
			int rows = ps.executeUpdate();
			if (rows > 0) {
				addFlag = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.closedb();
		}
		return addFlag;
	}

	public boolean update(Tdevicetype devicetype) {
		boolean altFlag = false;
		try {
			cn = new ConnDb().getcon();
			String sqlstr = "update tdevicetype set devicetypecode=?,devicetypename=?,addtime=? where id=? ";

			ps = cn.prepareStatement(sqlstr);
			ps.setString(1, devicetype.getDevicetypecode());
			ps.setString(2, devicetype.getDevicetypename());
			ps.setTimestamp(3,devicetype.getAddtime());
			ps.setLong(4, devicetype.getId());

			int rows = ps.executeUpdate();
			if (rows > 0) {
				altFlag = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.closedb();
		}
		return altFlag;
	}

	public boolean delete(String number) {
		boolean delFlag = false;
		try {
			cn = new ConnDb().getcon();

			String sqlstr = "delete from  tdevicetype  where id=" + number + "  ";
			ps = cn.prepareStatement(sqlstr);

			int rows = ps.executeUpdate();
			if (rows > 0) {
				delFlag = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.closedb();
		}
		return delFlag;
	}

}