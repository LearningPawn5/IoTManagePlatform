package iot.dao;

import iot.bean.Tdevice;
import iot.utils.ConnDb;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class DeviceDao {
	private Connection cn = null;
	private PreparedStatement ps = null;
	private ResultSet rs = null;
	public static final int PAGE_LENGTH = 5;

	public void closedb() {
		try {
			if (rs != null) {
				rs.close();
			}
			if (ps != null) {
				ps.close();
			}
			if (cn != null) {
				cn.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ArrayList<Tdevice> query(String conStr) {
		ArrayList<Tdevice> retlist = new ArrayList<Tdevice>();
		try {
			cn = new ConnDb().getcon();
			String sqlstr = "select  * from tdevice  ";
			if (conStr != "") {
				sqlstr = "select  * from tdevice where " + conStr + " ";
			}
			System.out.println(sqlstr);
			ps = cn.prepareStatement(sqlstr);

			rs = ps.executeQuery();
			while (rs.next()) {
				Tdevice temp = new Tdevice();
				temp.setId(rs.getLong("id"));
				temp.setDtid(rs.getLong("dtid"));
				temp.setDevicename(rs.getString("devicename"));
				temp.setDevicecode(rs.getString("devicecode"));
				temp.setDevicephoto(rs.getString("devicephoto"));
				temp.setDeviceconfig(rs.getString("deviceconfig"));
				temp.setDeviceenabled(rs.getInt("deviceenabled"));
				temp.setRemark(rs.getString("remark"));
				temp.setAddtime(rs.getTimestamp("addtime"));
				retlist.add(temp);
			}

		} catch (Exception e) {

			retlist = null;
			e.printStackTrace();
		} finally {
			this.closedb();
		}

		return retlist;
	}

	public ArrayList<Tdevice> queryPage(String conStr, int page) {
		ArrayList<Tdevice> retlist = new ArrayList<Tdevice>();
		try {
			cn = new ConnDb().getcon();

			int begin = (page - 1) * PAGE_LENGTH;

			String sqllimit = "  order by id desc limit " + begin + ","
					+ PAGE_LENGTH;

			String sqlstr = "select  * from tdevice " + sqllimit;
			if (conStr != "") {
				sqlstr = "select  * from tdevice where " + conStr + sqllimit;
			}
			System.out.println(sqlstr);
			ps = cn.prepareStatement(sqlstr);

			rs = ps.executeQuery();
			while (rs.next()) {
				Tdevice temp = new Tdevice();
				temp.setId(rs.getLong("id"));
				temp.setDtid(rs.getLong("dtid"));
				temp.setDevicename(rs.getString("devicename"));
				temp.setDevicecode(rs.getString("devicecode"));
				temp.setDevicephoto(rs.getString("devicephoto"));
				temp.setDeviceconfig(rs.getString("deviceconfig"));

				temp.setDeviceenabled(rs.getInt("deviceenabled"));
				temp.setRemark(rs.getString("remark"));
				temp.setAddtime(rs.getTimestamp("addtime"));
				retlist.add(temp);
			}

		} catch (Exception e) {
			retlist = null;
			e.printStackTrace();
		} finally {
			this.closedb();
		}

		return retlist;
	}

	public int count(String conStr) {
		int n = 0;
		try {
			cn = new ConnDb().getcon();
			String sqlstr = "select  count(*) from tdevice ";
			if (conStr != "") {
				sqlstr = "select  count(*) from tdevice where " + conStr;
			}
			// System.out.println(sqlstr);
			ps = cn.prepareStatement(sqlstr);

			rs = ps.executeQuery();
			if (rs.next()) {

				n = rs.getInt(1);

			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.closedb();
		}
		return n;
	}

	public boolean add(Tdevice device) {
		boolean addFlag = false;
		
		try {
			cn = new ConnDb().getcon();
			String sqlstr = "insert into tdevice(dtid,devicename,devicecode,devicephoto,addtime) values(?,?,?,?,?) ";
			ps = cn.prepareStatement(sqlstr);
			ps.setLong(1, device.getDtid());
			ps.setString(2, device.getDevicename());
			ps.setString(3, device.getDevicecode());
			ps.setString(4, device.getDevicephoto());
			ps.setTimestamp(5, device.getAddtime());
			int rows = ps.executeUpdate();
			if (rows > 0) {
				addFlag = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.closedb();
		}
		return addFlag;
	}

	public boolean update(Tdevice device) {
		boolean altFlag = false;
		try {
			cn = new ConnDb().getcon();
			String sqlstr = "update tdevice set dtid=?,devicename=?,devicecode=?,devicephoto=?,addtime=? where id=? ";

			ps = cn.prepareStatement(sqlstr);
			ps.setLong(1, device.getDtid());
			ps.setString(2, device.getDevicename());
			ps.setString(3, device.getDevicecode());
			ps.setString(4, device.getDevicephoto());
			ps.setTimestamp(5, device.getAddtime());
			ps.setLong(6, device.getId());
			System.out.print(sqlstr);
			int rows = ps.executeUpdate();
			if (rows > 0) {
				altFlag = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.closedb();
		}
		return altFlag;
	}

	public boolean delete(String number) {
		boolean delFlag = false;
		try {
			cn = new ConnDb().getcon();

			String sqlstr = "delete from  tdevice  where id=" + number + "  ";
			ps = cn.prepareStatement(sqlstr);

			int rows = ps.executeUpdate();
			if (rows > 0) {
				delFlag = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.closedb();
		}
		return delFlag;
	}

}
