package iot.bean;

import java.sql.Timestamp;

public class Tgate {

	private long id;
	private long pid;
	private String gatename;
	private String gateid;
	private int gateenabled;
	private  String remark;
	private Timestamp addtime;
	
	public Timestamp getAddtime() {
		return addtime;
	}
	public void setAddtime(Timestamp addtime) {
		this.addtime = addtime;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getPid() {
		return pid;
	}
	public void setPid(long pid) {
		this.pid = pid;
	}
	public String getGatename() {
		return gatename;
	}
	public void setGatename(String gatename) {
		this.gatename = gatename;
	}
	public String getGateid() {
		return gateid;
	}
	public void setGateid(String gateid) {
		this.gateid = gateid;
	}
	public int getGateenabled() {
		return gateenabled;
	}
	public void setGateenabled(int gateenabled) {
		this.gateenabled = gateenabled;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	
	
}
