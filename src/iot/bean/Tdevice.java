package iot.bean;

import java.sql.Timestamp;

public class Tdevice {
	private Long id;
	private String devicename;
	private String devicecode;
	private Long dtid;
	private String devicephoto;
	private String deviceconfig;
	private Timestamp addtime;
	private Integer deviceenabled;
	private String remark;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDevicename() {
		return devicename;
	}

	public void setDevicename(String devicename) {
		this.devicename = devicename;
	}

	public String getDevicecode() {
		return devicecode;
	}

	public void setDevicecode(String devicecode) {
		this.devicecode = devicecode;
	}

	public Long getDtid() {
		return dtid;
	}

	public void setDtid(Long dtid) {
		this.dtid = dtid;
	}

	public String getDevicephoto() {
		if (devicephoto == null || devicephoto.equals(""))
			this.devicephoto = "images/no-photo.gif";
		return devicephoto;
	}

	public void setDevicephoto(String devicephoto) {
		this.devicephoto = devicephoto;
	}

	public String getDeviceconfig() {
		return deviceconfig;
	}

	public void setDeviceconfig(String deviceconfig) {
		this.deviceconfig = deviceconfig;
	}

	public Timestamp getAddtime() {
		return addtime;
	}

	public void setAddtime(Timestamp addtime) {
		this.addtime = addtime;
	}

	public Integer getDeviceenabled() {
		return deviceenabled;
	}

	public void setDeviceenabled(Integer deviceenabled) {
		this.deviceenabled = deviceenabled;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}
