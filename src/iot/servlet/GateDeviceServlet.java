package iot.servlet;

import iot.bean.Tgatedevice;
import iot.dao.GateDeviceDao;
import iot.utils.CodeExchange;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class GateDeviceServlet extends HttpServlet {

	/**
	 * Constructor of the object.
	 */
	public GateDeviceServlet() {
		super();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	@SuppressWarnings("static-access")
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String action=request.getParameter("action");
		if(action!=null){
			if(action.equals("delete")){
				String id=request.getParameter("id");
				if(id!=null&&id.length()>0){
					GateDeviceDao gatedeviceDao=new GateDeviceDao();
					if(gatedeviceDao.delete(id)){
						request.setAttribute("msg", "删除成功！");
					}else{
						request.setAttribute("msg", "删除失败！");
					}					
					
				}else{
					response.sendRedirect("error.jsp");
					return;
				}
			}else if(action.equals("add")){
				 Tgatedevice gatedevice=new Tgatedevice();
				 gatedevice.setGid(Long.parseLong(request.getParameter("gid")));
				 gatedevice.setDid(Long.parseLong(request.getParameter("did")));
				 gatedevice.setClientdevicename(CodeExchange.chinese(request.getParameter("clientdevicename")));
				 gatedevice.setClientdeviceid(CodeExchange.chinese(request.getParameter("clientdeviceid")));
				 gatedevice.setAddtime(new Timestamp((new Date()).getTime()));
				GateDeviceDao gatedeviceDao=new GateDeviceDao();
				if(gatedeviceDao.add(gatedevice)){
					request.setAttribute("msg", "添加成功！");
				}else{
					request.setAttribute("msg", "添加失败！");
				}
			}else if(action.equals("update")){
				String id=request.getParameter("id");
				if(id!=null&&id.length()>0){
					GateDeviceDao gatedeviceDao=new GateDeviceDao();

					ArrayList<Tgatedevice> gatedevices=gatedeviceDao.query(" id=" + id);
					if(gatedevices!=null && gatedevices.size()>0){
						Tgatedevice gatedevice=gatedevices.get(0);
					request.setAttribute("gatedevice", gatedevice);

						request.getRequestDispatcher("gatedeviceedit.jsp").forward(request, response);
						return;
					}else{
						response.sendRedirect("error.jsp");
						return;
					}
					
				}else{
					response.sendRedirect("error.jsp");
					return;
				}
				
			}else if(action.equals("updateSave")){
				
				Tgatedevice gatedevice=new Tgatedevice();
				gatedevice.setGid(Long.parseLong(request.getParameter("gid")));
				gatedevice.setDid(Long.parseLong(request.getParameter("did")));
				gatedevice.setClientdevicename(CodeExchange.chinese(request.getParameter("clientdevicename")));
				gatedevice.setClientdeviceid(CodeExchange.chinese(request.getParameter("clientdeviceid")));
				gatedevice.setAddtime(new Timestamp((new Date()).getTime()));
				gatedevice.setId(Long.parseLong(request.getParameter("id")));
				GateDeviceDao gateDeviceDao=new GateDeviceDao();
				if(gateDeviceDao.update(gatedevice)){
					request.setAttribute("msg", "修改成功！");
				}else{
					request.setAttribute("msg", "修改失败！");
				}
				
			}	
			else{
				response.sendRedirect("error.jsp");
				return;
			}
		}	
		
		//查询条件
		String whereSQL="";
		String keyword=CodeExchange.chinese(request.getParameter("keyword"));
		String fieldname=request.getParameter("fieldname");
		if(keyword!=null&&fieldname!=null&&fieldname.length()>0&&keyword.length()>0){
			//id为整型
			if(fieldname.equals("id")){
				whereSQL=fieldname+"="+keyword;
				System.out.print(whereSQL);
			}
			//其他为字符串型，要加入单引号
			else
			{
				whereSQL=fieldname+" like '%"+keyword+"%' ";
			}
			//回传给页面显示查询条件，否则会清空
			request.setAttribute("fieldname", fieldname);
			request.setAttribute("keyword", keyword);
			
		}

		
		String page=request.getParameter("page");
		if(page==null){
			page="1";
		}
		GateDeviceDao gatedeviceDao=new GateDeviceDao();
		ArrayList<Tgatedevice> gatedevices =new ArrayList<Tgatedevice>();		
		gatedevices = gatedeviceDao.queryPage(whereSQL,Integer.parseInt(page));
		request.setAttribute("gatedevices", gatedevices);
		
		int total=gatedeviceDao.count(whereSQL);
		request.setAttribute("total", total/gatedeviceDao.PAGE_LENGTH + 1);
		
		request.setAttribute("currentPage", Integer.parseInt(page));
		request.getRequestDispatcher("gatedevicelist.jsp").forward(request, response);
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occurs
	 */
	public void init() throws ServletException {
		// Put your code here
	}

}
