package iot.servlet;

import iot.bean.Tproject;
import iot.dao.ProjectDao;
import iot.utils.CodeExchange;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class ProjectServlet extends HttpServlet {

	/**
	 * Constructor of the object.
	 */
	public ProjectServlet() {
		super();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	} 

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		this.doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	@SuppressWarnings("static-access")
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String action=request.getParameter("action");
		if(action!=null){
			if(action.equals("delete")){
				String id=request.getParameter("id");
				if(id!=null&&id.length()>0){
					ProjectDao projectDao=new ProjectDao();
					if(projectDao.delete(id)){
						request.setAttribute("msg", "删除成功！");
					}else{
						request.setAttribute("msg", "删除失败！");
					}					
					
				}else{
					response.sendRedirect("error.jsp");
					return;
				}
			}else if(action.equals("add")){
				 Tproject project=new Tproject();
				
				 project.setUid(Long.parseLong(request.getParameter("uid")));
				 project.setProjectname(CodeExchange.chinese(request.getParameter("projectname")));
				 project.setAddtime(new Timestamp((new Date()).getTime()));
				ProjectDao projectDao=new ProjectDao();
				if(projectDao.add(project)){
					request.setAttribute("msg", "添加成功！");
				}else{
					request.setAttribute("msg", "添加失败！");
				}
			}else if(action.equals("update")){

				String id=request.getParameter("id");
				if(id!=null&&id.length()>0){
					ProjectDao projectDao=new ProjectDao();

					ArrayList<Tproject> projects=projectDao.query(" id=" + id);
					if(projects!=null && projects.size()>0){
						Tproject project=projects.get(0);
					request.setAttribute("project", project);

						request.getRequestDispatcher("projectedit.jsp").forward(request, response);
						return;
					}else{
						response.sendRedirect("error.jsp");
						return;
					}
					
				}else{
					response.sendRedirect("error.jsp");
					return;
				}
				
			}else if(action.equals("updateSave")){
				
				Tproject project=new Tproject();
				project.setUid(Long.parseLong(request.getParameter("uid")));
				project.setProjectname(CodeExchange.chinese(request.getParameter("projectname")));
				project.setAddtime(new Timestamp((new Date()).getTime()));
				project.setId(Long.parseLong(request.getParameter("id")));
				ProjectDao projectDao=new ProjectDao();
				if(projectDao.update(project)){
					request.setAttribute("msg", "修改成功！");
				}else{
					request.setAttribute("msg", "修改失败！");                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
				}
				
			}	
			else{
				response.sendRedirect("error.jsp");
				return;
			}
		}	
		
		//查询条件
		String whereSQL="";
		String keyword=CodeExchange.chinese(request.getParameter("keyword"));
		String fieldname=request.getParameter("fieldname");
		if(keyword!=null&&fieldname!=null&&fieldname.length()>0&&keyword.length()>0){
			//id为整型
			if(fieldname.equals("id")){
				whereSQL=fieldname+"="+keyword;
				System.out.print(whereSQL);
			}
			//其他为字符串型，要加入单引号
			else
			{
				whereSQL=fieldname+" like '%"+keyword+"%' ";
			}
			//回传给页面显示查询条件，否则会清空
			request.setAttribute("fieldname", fieldname);
			request.setAttribute("keyword", keyword);
			
		}

		String page=request.getParameter("page");
		if(page==null){
			page="1";
		}
		ProjectDao projectDao=new ProjectDao();
		ArrayList<Tproject> projects =new ArrayList<Tproject>();		
		projects = projectDao.queryPage(whereSQL,Integer.parseInt(page));
		request.setAttribute("projects", projects);
		
		int total=projectDao.count(whereSQL);
		request.setAttribute("total", total/projectDao.PAGE_LENGTH + 1);
		
		request.setAttribute("currentPage", Integer.parseInt(page));
		request.getRequestDispatcher("projectlist.jsp").forward(request, response);
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occurs
	 */
	public void init() throws ServletException {
		// Put your code here
	}

}
