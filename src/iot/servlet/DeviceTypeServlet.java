package iot.servlet;

import iot.bean.Tdevicetype;
import iot.bean.Tuser;
import iot.dao.DeviceTypeDao;
import iot.dao.UserDao;
import iot.utils.CodeExchange;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

@SuppressWarnings({ "serial", "unused" })
public class DeviceTypeServlet extends HttpServlet {

	/**
	 * Constructor of the object.
	 */
	public DeviceTypeServlet() {
		super();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	} 

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		this.doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	@SuppressWarnings("static-access")
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String action=request.getParameter("action");
		if (action != null) {
			if (action.equals("delete")) {
				String id = request.getParameter("id");
				if (id != null && id.length() > 0) {
					DeviceTypeDao devicetypeDao = new DeviceTypeDao();
					if (devicetypeDao.delete(id)) {
						request.setAttribute("msg", "删除成功！");
					} else {
						request.setAttribute("msg", "删除失败！");
					}
				} else {
					response.sendRedirect("error.jsp");
					return;
				}
			} else if (action.equals("add")) {
				Tdevicetype devicetype = new Tdevicetype();
				devicetype.setDevicetypecode(CodeExchange.chinese(request.getParameter("devicetypecode")));
				devicetype.setDevicetypename(CodeExchange.chinese(request.getParameter("devicetypename")));
				devicetype.setAddtime(new Timestamp((new Date()).getTime()));
				DeviceTypeDao devicetypeDao = new DeviceTypeDao();
				if (devicetypeDao.add(devicetype)) {
					request.setAttribute("msg", "添加成功！");
				} else {
					request.setAttribute("msg", "添加失败！");
				}
			} else if (action.equals("update")) {
				String id = request.getParameter("id");
				if (id != null && id.length() > 0) {
					DeviceTypeDao devicetypeDao = new DeviceTypeDao();
					ArrayList<Tdevicetype> devicestypes = devicetypeDao.query(" id=" + id);
					if (devicestypes != null && devicestypes.size() > 0) {
						Tdevicetype devicetype = devicestypes.get(0);
						request.setAttribute("devicetype", devicetype);
						request.getRequestDispatcher("devicetypeedit.jsp").forward(request, response);
						return;
					} else {
						response.sendRedirect("error.jsp");
						return;
					}

				} else {
					response.sendRedirect("error.jsp");
					return;
				}

			} else if (action.equals("updateSave")) {

				Tdevicetype devicetype = new Tdevicetype();
				devicetype.setDevicetypename(CodeExchange.chinese(request.getParameter("devicetypename")));
				devicetype.setDevicetypecode(CodeExchange.chinese(request.getParameter("devicetypecode")));
				devicetype.setAddtime(new Timestamp((new Date()).getTime()));
				devicetype.setId(Long.parseLong(request.getParameter("id")));
				DeviceTypeDao devicetypeDao = new DeviceTypeDao();
				if (devicetypeDao.update(devicetype)) {
					request.setAttribute("msg", "修改成功！");
				} else {
					request.setAttribute("msg", "修改失败！");
				}

			} else {
				response.sendRedirect("error.jsp");
				return;
			}
		}
		

		String whereSQL="";
		String keyword=CodeExchange.chinese(request.getParameter("keyword"));
		String fieldname=request.getParameter("fieldname");
		if (keyword != null && fieldname != null && fieldname.length() > 0 && keyword.length() > 0) {

			if(fieldname.equals("id")){
				whereSQL=fieldname+"="+keyword;
				System.out.print(whereSQL);
			}

			else
			{
				whereSQL=fieldname+" like '%"+keyword+"%' ";
			}
			request.setAttribute("fieldname", fieldname);
			request.setAttribute("keyword", keyword);
			
		}

		String page=request.getParameter("page");
		if (page == null) {
			page = "1";
		}
		DeviceTypeDao devicetypeDao=new DeviceTypeDao();
		ArrayList<Tdevicetype> devicestype =new ArrayList<Tdevicetype>();		
		devicestype = devicetypeDao.queryPage(whereSQL,Integer.parseInt(page));
		request.setAttribute("devicestype", devicestype);
		
		int total=devicetypeDao.count(whereSQL);
		request.setAttribute("total", total/devicetypeDao.PAGE_LENGTH + 1);
		
		request.setAttribute("currentPage", Integer.parseInt(page));
		request.getRequestDispatcher("devicetypelist.jsp").forward(request, response);
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occurs
	 */
	public void init() throws ServletException {
		// Put your code here
	}

}
