package iot.servlet;

import iot.bean.*;
import iot.dao.*;
import iot.utils.CodeExchange;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class GateServlet extends HttpServlet {

	/**
	 * Constructor of the object.
	 */
	public GateServlet() {
		super();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request  the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException      if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		this.doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request  the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException      if an error occurred
	 */
	@SuppressWarnings("static-access")
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String action = request.getParameter("action");
		if (action != null) {
			if (action.equals("delete")) {
				String id = request.getParameter("id");
				if (id != null && id.length() > 0) {
					GateDao gateDao = new GateDao();
					if (gateDao.delete(id)) {
						request.setAttribute("msg", "删除成功！");
					} else {
						request.setAttribute("msg", "删除失败！");
					}

				} else {
					response.sendRedirect("error.jsp");
					return;
				}
			} else if (action.equals("add")) {
				Tgate gate = new Tgate();
				gate.setGateid(CodeExchange.chinese(request.getParameter("gateid")));
				gate.setGatename(CodeExchange.chinese(request.getParameter("gatename")));
				gate.setPid(Long.parseLong(request.getParameter("pid")));
				gate.setAddtime(new Timestamp((new Date()).getTime()));
				GateDao gateDao = new GateDao();
				if (gateDao.add(gate)) {
					request.setAttribute("msg", "添加成功！");
				} else {
					request.setAttribute("msg", "添加失败！");
				}
			} else if (action.equals("update")) {
				String id = request.getParameter("id");
				if (id != null && id.length() > 0) {
					GateDao gateDao = new GateDao();

					ArrayList<Tgate> gates = gateDao.query(" id=" + id);
					if (gates != null && gates.size() > 0) {
						Tgate gate = gates.get(0);
						request.setAttribute("gate", gate);
						request.getRequestDispatcher("gateedit.jsp").forward(request, response);
						return;
					} else {
						response.sendRedirect("error.jsp");
						return;
					}

				} else {
					response.sendRedirect("error.jsp");
					return;
				}

			} else if (action.equals("updateSave")) {

				Tgate gate = new Tgate();
				gate.setGateid(CodeExchange.chinese(request.getParameter("gateid")));
				gate.setGatename(CodeExchange.chinese(request.getParameter("gatename")));
				gate.setPid(Long.parseLong(request.getParameter("pid")));
				gate.setAddtime(new Timestamp((new Date()).getTime()));
				gate.setId(Long.parseLong(request.getParameter("id")));
				GateDao gateDao = new GateDao();
				if (gateDao.update(gate)) {
					request.setAttribute("msg", "修改成功！");
				} else {
					request.setAttribute("msg", "修改失败！");
				}

			} else {
				response.sendRedirect("error.jsp");
				return;
			}
		}

		String whereSQL = "";
		String keyword = CodeExchange.chinese(request.getParameter("keyword"));
		String fieldname = request.getParameter("fieldname");
		if (keyword != null && fieldname != null && fieldname.length() > 0 && keyword.length() > 0) {
			if (fieldname.equals("id")) {
				whereSQL = fieldname + "=" + keyword;
				System.out.print(whereSQL);
			} else {
				whereSQL = fieldname + " like '%" + keyword + "%' ";
			}
			request.setAttribute("fieldname", fieldname);
			request.setAttribute("keyword", keyword);

		}

		String page = request.getParameter("page");
		if (page == null) {
			page = "1";
		}
		GateDao gateDao = new GateDao();
		ArrayList<Tgate> gates = new ArrayList<Tgate>();
		gates = gateDao.queryPage(whereSQL, Integer.parseInt(page));
		request.setAttribute("gates", gates);

		int total = gateDao.count(whereSQL);
		request.setAttribute("total", total / gateDao.PAGE_LENGTH + 1);

		request.setAttribute("currentPage", Integer.parseInt(page));
		request.getRequestDispatcher("gatelist.jsp").forward(request, response);
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occurs
	 */
	public void init() throws ServletException {
		// Put your code here
	}

}
