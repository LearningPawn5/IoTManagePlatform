/*
Navicat MySQL Data Transfer

Source Server         : MySQL
Source Server Version : 50629
Source Host           : localhost:3306
Source Database       : iotdb

Target Server Type    : MYSQL
Target Server Version : 50629
File Encoding         : 65001

Date: 2021-01-12 23:33:02
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `talarmdata`
-- ----------------------------
DROP TABLE IF EXISTS `talarmdata`;
CREATE TABLE `talarmdata` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `gdid` bigint(20) DEFAULT NULL COMMENT '终端设备ID,关联tgatedevice',
  `alarmtime` datetime NOT NULL COMMENT '报警时间',
  `alarmdata` text NOT NULL COMMENT '报警数据：JSON格式',
  `pushresult` varchar(1024) DEFAULT NULL COMMENT 'APP消息推送结果：预留备用',
  `proccessresult` varchar(1024) DEFAULT NULL COMMENT '报警处理结果',
  `remark` varchar(1024) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  KEY `ex_gdid` (`gdid`),
  CONSTRAINT `ex_gdid` FOREIGN KEY (`gdid`) REFERENCES `tgatedevice` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='终端设备报警数据';

-- ----------------------------
-- Records of talarmdata
-- ----------------------------

-- ----------------------------
-- Table structure for `tdevice`
-- ----------------------------
DROP TABLE IF EXISTS `tdevice`;
CREATE TABLE `tdevice` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `dtid` bigint(20) DEFAULT NULL COMMENT '设备类型id',
  `devicecode` varchar(50) NOT NULL COMMENT '设备型号：终端通讯备用，必须唯一',
  `devicename` varchar(256) NOT NULL COMMENT '设备名称',
  `devicephoto` varchar(1024) DEFAULT 'images/no-photo.gif' COMMENT '设备图片',
  `deviceconfig` text COMMENT '设备参数配置：JSON格式（可以覆盖类型中的参数设定）',
  `deviceenabled` int(11) DEFAULT NULL COMMENT '是否启用',
  `remark` varchar(1024) DEFAULT NULL COMMENT '备注',
  `addtime` datetime DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `ex_dtid` (`dtid`),
  CONSTRAINT `ex_dtid` FOREIGN KEY (`dtid`) REFERENCES `tdevicetype` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='物联网设备';

-- ----------------------------
-- Records of tdevice
-- ----------------------------
INSERT INTO `tdevice` VALUES ('3', '1', 'DHT11', '温湿度传感器', 'photos/DHT11.png', '{\"temp\":\"int\",\"humi\":\"int\"}', '1', null, '2021-01-05 01:20:05');
INSERT INTO `tdevice` VALUES ('4', '2', 'RGBLED', '三色LED灯', 'photos/RGBLED.png', '{\"power\":[\"on\",\"off\"],\"color\":[\"r\",\"g\",\"b\"]}', '1', null, '2021-01-05 01:20:20');
INSERT INTO `tdevice` VALUES ('6', '3', 'WebCam', '监控摄像头', 'photos/WebCam.png', null, null, null, '2021-01-05 01:17:52');
INSERT INTO `tdevice` VALUES ('7', '2', 'E-Door', '电子门', 'photos/E-Door.png', null, null, null, '2021-01-05 01:23:05');
INSERT INTO `tdevice` VALUES ('8', '4', 'LinuxSimulator', 'Linux模拟器', 'photos/LinuxSimulator.png', null, null, null, '2021-01-05 01:26:13');

-- ----------------------------
-- Table structure for `tdevicetype`
-- ----------------------------
DROP TABLE IF EXISTS `tdevicetype`;
CREATE TABLE `tdevicetype` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `devicetypecode` varchar(50) NOT NULL COMMENT '设备类型编码：终端通讯备用，必须唯一',
  `devicetypename` varchar(256) NOT NULL COMMENT '设备类型名称：感知设备，控制设备，监控设备',
  `devicetypeconfig` text COMMENT '设备类型参数配置：JSON格式',
  `remark` varchar(1024) DEFAULT NULL COMMENT '备注',
  `addtime` datetime DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='物联网设备类型';

-- ----------------------------
-- Records of tdevicetype
-- ----------------------------
INSERT INTO `tdevicetype` VALUES ('1', 'device', '感知设备', null, null, '2021-01-03 18:09:39');
INSERT INTO `tdevicetype` VALUES ('2', 'control', '控制设备', null, null, '2021-01-03 18:09:33');
INSERT INTO `tdevicetype` VALUES ('3', 'moniter', '监控设备', null, null, '2021-01-03 19:13:09');
INSERT INTO `tdevicetype` VALUES ('4', 'simulator', '模拟设备', null, null, '2021-01-05 01:25:24');

-- ----------------------------
-- Table structure for `tgate`
-- ----------------------------
DROP TABLE IF EXISTS `tgate`;
CREATE TABLE `tgate` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `pid` bigint(20) DEFAULT NULL COMMENT '项目ID',
  `gatename` varchar(256) NOT NULL COMMENT '网关名称',
  `gateid` varchar(50) NOT NULL COMMENT '网关编号：通讯端使用，必须唯一，可以采用UUID',
  `gateenabled` int(11) DEFAULT '1' COMMENT '是否启用',
  `remark` varchar(1024) DEFAULT NULL COMMENT '备注',
  `addtime` datetime DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='物联网通讯网关';

-- ----------------------------
-- Records of tgate
-- ----------------------------
INSERT INTO `tgate` VALUES ('1', '1', '智慧网关1', 'gate001', '1', null, '2021-01-04 19:27:39');
INSERT INTO `tgate` VALUES ('2', '2', 'Tom\'s GateWay 001', 'gateTom001', '1', null, '2021-01-04 19:28:16');
INSERT INTO `tgate` VALUES ('3', '2', 'Tom\'s GateWay 002', 'gateTom002', '1', null, '2021-01-05 00:53:50');

-- ----------------------------
-- Table structure for `tgatedevice`
-- ----------------------------
DROP TABLE IF EXISTS `tgatedevice`;
CREATE TABLE `tgatedevice` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `gid` bigint(20) DEFAULT NULL COMMENT '网关ID',
  `did` bigint(20) DEFAULT NULL COMMENT '设备ID',
  `clientdevicename` varchar(256) DEFAULT NULL COMMENT '终端设备名称',
  `clientdeviceid` varchar(256) NOT NULL COMMENT '终端设备编号：通讯时候使用，必须唯一，可以采用UUID',
  `clientdeviceconfig` text COMMENT '终端设备参数：JSON格式（包含报警上下限等）',
  `clientdevicestate` varchar(2048) DEFAULT NULL COMMENT '终端设备状态：备用',
  `clientdeviceenabled` int(11) DEFAULT NULL COMMENT '是否启用',
  `remark` varchar(1024) DEFAULT NULL COMMENT '备注',
  `addtime` datetime DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `ex_gid` (`gid`),
  KEY `ex_did` (`did`),
  CONSTRAINT `ex_did` FOREIGN KEY (`did`) REFERENCES `tdevice` (`id`),
  CONSTRAINT `ex_gid` FOREIGN KEY (`gid`) REFERENCES `tgate` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='物联网终端设备';

-- ----------------------------
-- Records of tgatedevice
-- ----------------------------
INSERT INTO `tgatedevice` VALUES ('1', '2', '3', '温湿度', 'DHT11', null, null, '1', null, '2021-01-04 22:47:33');
INSERT INTO `tgatedevice` VALUES ('2', '2', '4', '三色LED灯', 'RGBLED', null, null, null, null, '2021-01-05 00:40:11');
INSERT INTO `tgatedevice` VALUES ('3', '2', '6', 'Tom Bedroom WebCam', 'WebCam001', null, null, null, null, '2021-01-05 00:59:40');
INSERT INTO `tgatedevice` VALUES ('4', '3', '7', '大堂正门', 'EDoor001', null, null, null, null, '2021-01-05 01:23:58');
INSERT INTO `tgatedevice` VALUES ('5', '3', '8', '大厅电脑', 'LivingRoomPC', null, null, null, null, '2021-01-05 01:26:54');

-- ----------------------------
-- Table structure for `thistorydata`
-- ----------------------------
DROP TABLE IF EXISTS `thistorydata`;
CREATE TABLE `thistorydata` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `gdid` bigint(20) DEFAULT NULL COMMENT '终端设备ID,关联tgatedevice',
  `recordtime` datetime NOT NULL COMMENT '记录时间',
  `recorddata` text NOT NULL COMMENT '数据JSON格式',
  `remark` varchar(1024) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=731 DEFAULT CHARSET=utf8 COMMENT='历史记录按照终端设备记录';

-- ----------------------------
-- Records of thistorydata
-- ----------------------------
INSERT INTO `thistorydata` VALUES ('18', '1010001', '2019-12-21 15:11:38', '{\"humi\":\"15\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('19', '1010001', '2019-12-21 15:11:43', '{\"humi\":\"13\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('20', '1010001', '2019-12-21 15:12:38', '{\"humi\":\"14\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('21', '1010001', '2019-12-21 15:12:43', '{\"humi\":\"13\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('22', '1010001', '2019-12-21 15:12:48', '{\"humi\":\"12\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('23', '1010001', '2019-12-21 15:12:53', '{\"humi\":\"18\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('24', '1010001', '2019-12-21 15:12:58', '{\"humi\":\"13\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('25', '1010001', '2019-12-21 15:13:03', '{\"humi\":\"10\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('26', '1010001', '2019-12-21 15:13:08', '{\"humi\":\"14\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('27', '1010001', '2019-12-21 15:13:13', '{\"humi\":\"20\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('28', '1010001', '2019-12-21 15:13:18', '{\"humi\":\"16\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('29', '1010001', '2019-12-21 15:13:23', '{\"humi\":\"10\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('30', '1010001', '2019-12-21 15:13:28', '{\"humi\":\"14\",\"temp\":\"19\"}', null);
INSERT INTO `thistorydata` VALUES ('31', '1010001', '2019-12-21 15:13:33', '{\"humi\":\"11\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('32', '1010001', '2019-12-21 15:13:38', '{\"humi\":\"10\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('33', '1010001', '2019-12-21 15:13:43', '{\"humi\":\"12\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('34', '1010001', '2019-12-21 15:13:48', '{\"humi\":\"13\",\"temp\":\"20\"}', null);
INSERT INTO `thistorydata` VALUES ('35', '1010001', '2019-12-21 15:13:53', '{\"humi\":\"12\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('36', '1010001', '2019-12-21 15:13:58', '{\"humi\":\"15\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('37', '1010001', '2019-12-21 15:14:03', '{\"humi\":\"17\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('38', '1010001', '2019-12-21 15:14:08', '{\"humi\":\"20\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('39', '1010001', '2019-12-21 15:14:13', '{\"humi\":\"15\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('40', '1010001', '2019-12-21 15:14:18', '{\"humi\":\"18\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('41', '1010001', '2019-12-21 15:14:23', '{\"humi\":\"13\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('42', '1010001', '2019-12-21 15:14:28', '{\"humi\":\"19\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('43', '1010001', '2019-12-21 15:14:33', '{\"humi\":\"12\",\"temp\":\"19\"}', null);
INSERT INTO `thistorydata` VALUES ('44', '1010001', '2019-12-21 15:14:38', '{\"humi\":\"10\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('45', '1010001', '2019-12-21 15:14:43', '{\"humi\":\"20\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('46', '1010001', '2019-12-21 15:14:48', '{\"humi\":\"14\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('47', '1010001', '2019-12-21 15:14:53', '{\"humi\":\"18\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('48', '1010001', '2019-12-21 15:14:58', '{\"humi\":\"10\",\"temp\":\"20\"}', null);
INSERT INTO `thistorydata` VALUES ('49', '1010001', '2019-12-21 15:15:03', '{\"humi\":\"16\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('50', '1010001', '2019-12-21 15:15:08', '{\"humi\":\"10\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('51', '1010001', '2019-12-21 15:15:13', '{\"humi\":\"10\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('52', '1010001', '2019-12-21 15:15:18', '{\"humi\":\"16\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('53', '1010001', '2019-12-21 15:15:23', '{\"humi\":\"20\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('54', '1010001', '2019-12-21 15:15:28', '{\"humi\":\"11\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('55', '1010001', '2019-12-21 15:15:33', '{\"humi\":\"13\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('56', '1010001', '2019-12-21 15:15:38', '{\"humi\":\"14\",\"temp\":\"20\"}', null);
INSERT INTO `thistorydata` VALUES ('57', '1010001', '2019-12-21 15:15:43', '{\"humi\":\"16\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('58', '1010001', '2019-12-21 15:15:48', '{\"humi\":\"15\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('59', '1010001', '2019-12-21 15:15:53', '{\"humi\":\"16\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('60', '1010001', '2019-12-21 15:15:58', '{\"humi\":\"12\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('61', '1010001', '2019-12-21 15:16:03', '{\"humi\":\"16\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('62', '1010001', '2019-12-21 15:16:08', '{\"humi\":\"19\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('63', '1010001', '2019-12-21 15:16:13', '{\"humi\":\"14\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('64', '1010001', '2019-12-21 15:16:57', '{\"humi\":\"14\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('65', '1010001', '2019-12-21 15:17:02', '{\"humi\":\"19\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('66', '1010001', '2019-12-21 15:17:07', '{\"humi\":\"11\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('67', '1010001', '2019-12-21 15:17:12', '{\"humi\":\"15\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('68', '1010001', '2019-12-21 15:17:17', '{\"humi\":\"17\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('69', '1010001', '2019-12-21 15:17:22', '{\"humi\":\"14\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('70', '1010001', '2019-12-21 15:17:27', '{\"humi\":\"15\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('71', '1010001', '2019-12-21 15:17:32', '{\"humi\":\"13\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('72', '1010001', '2019-12-21 15:17:37', '{\"humi\":\"12\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('73', '1010001', '2019-12-21 15:17:42', '{\"humi\":\"16\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('74', '1010001', '2019-12-21 15:17:47', '{\"humi\":\"14\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('75', '1010001', '2019-12-21 15:17:52', '{\"humi\":\"12\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('76', '1010001', '2019-12-21 15:17:57', '{\"humi\":\"13\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('77', '1010001', '2019-12-21 15:18:02', '{\"humi\":\"10\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('78', '1010001', '2019-12-21 15:18:07', '{\"humi\":\"20\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('79', '1010001', '2019-12-21 15:18:12', '{\"humi\":\"10\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('80', '1010001', '2019-12-21 15:18:17', '{\"humi\":\"10\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('81', '1010001', '2019-12-21 15:18:22', '{\"humi\":\"11\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('82', '1010001', '2019-12-21 15:18:27', '{\"humi\":\"12\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('83', '1010001', '2019-12-21 15:18:32', '{\"humi\":\"12\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('84', '1010001', '2019-12-21 15:18:38', '{\"humi\":\"17\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('85', '1010001', '2019-12-21 15:18:43', '{\"humi\":\"14\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('86', '1010001', '2019-12-21 15:18:48', '{\"humi\":\"19\",\"temp\":\"19\"}', null);
INSERT INTO `thistorydata` VALUES ('87', '1010001', '2019-12-21 15:18:56', '{\"humi\":\"18\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('88', '1010001', '2019-12-21 15:19:16', '{\"humi\":\"13\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('89', '1010001', '2019-12-21 15:19:21', '{\"humi\":\"17\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('90', '1010001', '2019-12-21 15:19:26', '{\"humi\":\"12\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('91', '1010001', '2019-12-21 15:19:31', '{\"humi\":\"10\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('92', '1010001', '2019-12-21 15:19:36', '{\"humi\":\"16\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('93', '1010001', '2019-12-21 15:19:41', '{\"humi\":\"14\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('94', '1010001', '2019-12-21 15:19:46', '{\"humi\":\"14\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('95', '1010001', '2019-12-21 15:19:51', '{\"humi\":\"13\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('96', '1010001', '2019-12-21 15:19:56', '{\"humi\":\"10\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('97', '1010001', '2019-12-21 15:20:01', '{\"humi\":\"14\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('98', '1010001', '2019-12-21 15:20:06', '{\"humi\":\"10\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('99', '1010001', '2019-12-21 15:20:11', '{\"humi\":\"18\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('100', '1010001', '2019-12-21 15:20:16', '{\"humi\":\"10\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('101', '1010001', '2019-12-21 15:20:21', '{\"humi\":\"15\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('102', '1010001', '2019-12-21 15:20:26', '{\"humi\":\"10\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('103', '1010001', '2019-12-21 15:20:31', '{\"humi\":\"16\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('104', '1010001', '2019-12-21 15:56:01', '{\"humi\":\"18\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('105', '1010001', '2019-12-21 15:56:06', '{\"humi\":\"11\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('106', '1010001', '2019-12-21 15:56:11', '{\"humi\":\"10\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('107', '1010001', '2019-12-21 15:56:16', '{\"humi\":\"12\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('108', '1010001', '2019-12-21 15:56:21', '{\"humi\":\"10\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('109', '1010001', '2019-12-21 15:56:26', '{\"humi\":\"10\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('110', '1010001', '2019-12-21 15:56:31', '{\"humi\":\"19\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('111', '1010001', '2019-12-21 15:56:36', '{\"humi\":\"20\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('112', '1010001', '2019-12-21 15:56:41', '{\"humi\":\"18\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('113', '1010001', '2019-12-21 15:56:46', '{\"humi\":\"15\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('114', '1010001', '2019-12-21 15:56:51', '{\"humi\":\"18\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('115', '1010001', '2019-12-21 15:56:56', '{\"humi\":\"10\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('116', '1010001', '2019-12-21 15:57:01', '{\"humi\":\"15\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('117', '1010001', '2019-12-21 15:57:06', '{\"humi\":\"16\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('118', '1010001', '2019-12-21 15:57:11', '{\"humi\":\"11\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('119', '1010001', '2019-12-21 15:57:16', '{\"humi\":\"13\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('120', '1010001', '2019-12-21 15:57:21', '{\"humi\":\"14\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('121', '1010001', '2019-12-21 15:57:26', '{\"humi\":\"11\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('122', '1010001', '2019-12-21 15:57:31', '{\"humi\":\"13\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('123', '1010001', '2019-12-21 15:57:36', '{\"humi\":\"14\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('124', '1010001', '2019-12-21 15:57:41', '{\"humi\":\"15\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('125', '1010001', '2019-12-21 15:57:46', '{\"humi\":\"10\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('126', '1010001', '2019-12-21 15:57:51', '{\"humi\":\"11\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('127', '1010001', '2019-12-21 15:57:56', '{\"humi\":\"11\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('128', '1010001', '2019-12-21 15:58:01', '{\"humi\":\"17\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('129', '1010001', '2019-12-21 15:58:06', '{\"humi\":\"20\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('130', '1010001', '2019-12-21 15:58:11', '{\"humi\":\"11\",\"temp\":\"19\"}', null);
INSERT INTO `thistorydata` VALUES ('131', '1010001', '2019-12-21 15:58:16', '{\"humi\":\"18\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('132', '1010001', '2019-12-21 15:58:21', '{\"humi\":\"15\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('133', '1010001', '2019-12-21 15:58:26', '{\"humi\":\"17\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('134', '1010001', '2019-12-21 15:58:31', '{\"humi\":\"12\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('135', '1010001', '2019-12-21 15:58:36', '{\"humi\":\"11\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('136', '1010001', '2019-12-21 15:58:42', '{\"humi\":\"14\",\"temp\":\"19\"}', null);
INSERT INTO `thistorydata` VALUES ('137', '1010001', '2019-12-21 15:58:47', '{\"humi\":\"12\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('138', '1010001', '2019-12-21 15:58:52', '{\"humi\":\"19\",\"temp\":\"19\"}', null);
INSERT INTO `thistorydata` VALUES ('139', '1010001', '2019-12-21 15:58:57', '{\"humi\":\"16\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('140', '1010001', '2019-12-21 15:59:02', '{\"humi\":\"14\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('141', '1010001', '2019-12-21 15:59:07', '{\"humi\":\"13\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('142', '1010001', '2019-12-21 15:59:12', '{\"humi\":\"11\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('143', '1010001', '2019-12-21 15:59:17', '{\"humi\":\"10\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('144', '1010001', '2019-12-21 15:59:22', '{\"humi\":\"11\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('145', '1010001', '2019-12-21 15:59:27', '{\"humi\":\"18\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('146', '1010001', '2019-12-21 15:59:32', '{\"humi\":\"12\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('147', '1010001', '2019-12-21 15:59:37', '{\"humi\":\"11\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('148', '1010001', '2019-12-21 15:59:42', '{\"humi\":\"15\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('149', '1010001', '2019-12-21 15:59:47', '{\"humi\":\"17\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('150', '1010001', '2019-12-21 15:59:52', '{\"humi\":\"13\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('151', '1010001', '2019-12-21 15:59:57', '{\"humi\":\"15\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('152', '1010001', '2019-12-21 16:00:02', '{\"humi\":\"10\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('153', '1010001', '2019-12-21 16:00:07', '{\"humi\":\"17\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('154', '1010001', '2019-12-21 16:00:12', '{\"humi\":\"10\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('155', '1010001', '2019-12-21 16:00:17', '{\"humi\":\"10\",\"temp\":\"19\"}', null);
INSERT INTO `thistorydata` VALUES ('156', '1010001', '2019-12-21 16:00:22', '{\"humi\":\"11\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('157', '1010001', '2019-12-21 16:00:27', '{\"humi\":\"12\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('158', '1010001', '2019-12-21 16:00:32', '{\"humi\":\"12\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('159', '1010001', '2019-12-21 16:00:37', '{\"humi\":\"17\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('160', '1010001', '2019-12-21 16:00:42', '{\"humi\":\"11\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('161', '1010001', '2019-12-21 16:00:47', '{\"humi\":\"14\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('162', '1010001', '2019-12-21 16:00:52', '{\"humi\":\"18\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('163', '1010001', '2019-12-21 16:00:57', '{\"humi\":\"17\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('164', '1010001', '2019-12-21 21:09:05', '{\"humi\":\"20\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('165', '1010001', '2019-12-21 21:09:10', '{\"humi\":\"10\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('166', '1010001', '2019-12-21 21:09:15', '{\"humi\":\"15\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('167', '1010001', '2019-12-21 21:09:20', '{\"humi\":\"13\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('168', '1010001', '2019-12-21 21:09:25', '{\"humi\":\"15\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('169', '1010001', '2019-12-21 21:09:30', '{\"humi\":\"18\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('170', '1010001', '2019-12-21 21:09:35', '{\"humi\":\"16\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('171', '1010001', '2019-12-21 21:09:40', '{\"humi\":\"16\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('172', '1010001', '2019-12-21 21:09:45', '{\"humi\":\"12\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('173', '1010001', '2019-12-21 21:09:50', '{\"humi\":\"16\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('174', '1010001', '2019-12-21 21:09:55', '{\"humi\":\"11\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('175', '1010001', '2019-12-21 21:10:00', '{\"humi\":\"11\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('176', '1010001', '2019-12-21 21:10:05', '{\"humi\":\"11\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('177', '1010001', '2019-12-21 21:10:10', '{\"humi\":\"15\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('178', '1010001', '2019-12-21 21:10:15', '{\"humi\":\"19\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('179', '1010001', '2019-12-21 21:10:20', '{\"humi\":\"20\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('180', '1010001', '2019-12-21 21:10:25', '{\"humi\":\"15\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('181', '1010001', '2019-12-21 21:10:30', '{\"humi\":\"18\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('182', '1010001', '2019-12-21 21:10:35', '{\"humi\":\"16\",\"temp\":\"19\"}', null);
INSERT INTO `thistorydata` VALUES ('183', '1010001', '2019-12-21 21:10:40', '{\"humi\":\"11\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('184', '1010001', '2019-12-21 21:10:45', '{\"humi\":\"11\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('185', '1010001', '2019-12-21 21:10:50', '{\"humi\":\"13\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('186', '1010001', '2019-12-21 21:10:55', '{\"humi\":\"14\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('187', '1010001', '2019-12-21 21:11:00', '{\"humi\":\"15\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('188', '1010001', '2019-12-21 21:11:05', '{\"humi\":\"13\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('189', '1010001', '2019-12-21 21:11:10', '{\"humi\":\"12\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('190', '1010001', '2019-12-21 21:11:15', '{\"humi\":\"14\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('191', '1010001', '2019-12-21 21:11:20', '{\"humi\":\"15\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('192', '1010001', '2019-12-21 21:11:25', '{\"humi\":\"14\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('193', '1010001', '2019-12-21 21:11:30', '{\"humi\":\"15\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('194', '1010001', '2019-12-21 21:11:35', '{\"humi\":\"16\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('195', '1010001', '2019-12-21 21:11:40', '{\"humi\":\"16\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('196', '1010001', '2019-12-21 21:11:45', '{\"humi\":\"15\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('197', '1010001', '2019-12-21 21:11:50', '{\"humi\":\"12\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('198', '1010001', '2019-12-21 21:11:55', '{\"humi\":\"11\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('199', '1010001', '2019-12-21 21:12:00', '{\"humi\":\"11\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('200', '1010001', '2019-12-21 21:12:05', '{\"humi\":\"19\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('201', '1010001', '2019-12-21 21:12:10', '{\"humi\":\"17\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('202', '1010001', '2019-12-21 21:12:15', '{\"humi\":\"13\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('203', '1010001', '2019-12-21 21:12:20', '{\"humi\":\"17\",\"temp\":\"20\"}', null);
INSERT INTO `thistorydata` VALUES ('204', '1010001', '2019-12-21 21:12:25', '{\"humi\":\"13\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('205', '1010001', '2019-12-21 21:12:30', '{\"humi\":\"16\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('206', '1010001', '2019-12-21 21:12:35', '{\"humi\":\"10\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('207', '1010001', '2019-12-21 21:12:40', '{\"humi\":\"12\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('208', '1010001', '2019-12-21 21:12:45', '{\"humi\":\"13\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('209', '1010001', '2019-12-21 21:12:50', '{\"humi\":\"20\",\"temp\":\"20\"}', null);
INSERT INTO `thistorydata` VALUES ('210', '1010001', '2019-12-21 21:12:55', '{\"humi\":\"14\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('211', '1010001', '2019-12-21 21:13:00', '{\"humi\":\"16\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('212', '1010001', '2019-12-21 21:13:05', '{\"humi\":\"16\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('213', '1010001', '2019-12-21 21:13:10', '{\"humi\":\"15\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('214', '1010001', '2019-12-21 21:13:15', '{\"humi\":\"14\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('215', '1010001', '2019-12-21 21:13:20', '{\"humi\":\"13\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('216', '1010001', '2019-12-21 21:13:25', '{\"humi\":\"16\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('217', '1010001', '2019-12-21 21:13:30', '{\"humi\":\"12\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('218', '1010001', '2019-12-21 21:13:35', '{\"humi\":\"13\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('219', '1010001', '2019-12-21 21:13:40', '{\"humi\":\"11\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('220', '1010001', '2019-12-21 21:13:45', '{\"humi\":\"15\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('221', '1010001', '2019-12-21 21:13:50', '{\"humi\":\"15\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('222', '1010001', '2019-12-21 21:13:55', '{\"humi\":\"15\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('223', '1010001', '2019-12-21 21:14:00', '{\"humi\":\"14\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('224', '1010001', '2019-12-21 21:14:05', '{\"humi\":\"20\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('225', '1010001', '2019-12-21 21:14:10', '{\"humi\":\"11\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('226', '1010001', '2019-12-21 21:14:15', '{\"humi\":\"14\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('227', '1010001', '2019-12-21 21:14:20', '{\"humi\":\"20\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('228', '1010001', '2019-12-21 21:14:25', '{\"humi\":\"18\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('229', '1010001', '2019-12-21 21:14:30', '{\"humi\":\"18\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('230', '1010001', '2019-12-21 21:14:35', '{\"humi\":\"16\",\"temp\":\"20\"}', null);
INSERT INTO `thistorydata` VALUES ('231', '1010001', '2019-12-21 21:14:40', '{\"humi\":\"19\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('232', '1010001', '2019-12-21 21:14:45', '{\"humi\":\"18\",\"temp\":\"19\"}', null);
INSERT INTO `thistorydata` VALUES ('233', '1010001', '2019-12-21 21:14:50', '{\"humi\":\"15\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('234', '1010001', '2019-12-21 21:14:55', '{\"humi\":\"14\",\"temp\":\"19\"}', null);
INSERT INTO `thistorydata` VALUES ('235', '1010001', '2019-12-21 21:15:00', '{\"humi\":\"10\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('236', '1010001', '2019-12-21 21:15:05', '{\"humi\":\"18\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('237', '1010001', '2019-12-21 21:15:10', '{\"humi\":\"10\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('238', '1010001', '2019-12-21 21:15:15', '{\"humi\":\"17\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('239', '1010001', '2019-12-21 21:15:20', '{\"humi\":\"11\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('240', '1010001', '2019-12-21 21:15:25', '{\"humi\":\"17\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('241', '1010001', '2019-12-21 21:15:30', '{\"humi\":\"13\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('242', '1010001', '2019-12-21 21:15:35', '{\"humi\":\"18\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('243', '1010001', '2019-12-21 21:15:40', '{\"humi\":\"15\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('244', '1010001', '2019-12-21 21:15:45', '{\"humi\":\"11\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('245', '1010001', '2019-12-21 21:15:50', '{\"humi\":\"16\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('246', '1010001', '2019-12-21 21:15:55', '{\"humi\":\"17\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('247', '1010001', '2019-12-21 21:16:00', '{\"humi\":\"10\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('248', '1010001', '2019-12-21 21:16:05', '{\"humi\":\"16\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('249', '1010001', '2019-12-21 21:16:10', '{\"humi\":\"15\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('250', '1010001', '2019-12-21 21:16:15', '{\"humi\":\"10\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('251', '1010001', '2019-12-21 21:16:20', '{\"humi\":\"17\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('252', '1010001', '2019-12-21 21:16:25', '{\"humi\":\"13\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('253', '1010001', '2019-12-21 21:16:30', '{\"humi\":\"13\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('254', '1010001', '2019-12-21 21:16:35', '{\"humi\":\"14\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('255', '1010001', '2019-12-21 21:16:40', '{\"humi\":\"20\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('256', '1010001', '2019-12-21 21:16:45', '{\"humi\":\"20\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('257', '1010001', '2019-12-21 21:16:50', '{\"humi\":\"17\",\"temp\":\"20\"}', null);
INSERT INTO `thistorydata` VALUES ('258', '1010001', '2019-12-21 21:16:55', '{\"humi\":\"13\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('259', '1010001', '2019-12-21 21:17:00', '{\"humi\":\"18\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('260', '1010001', '2019-12-21 21:17:05', '{\"humi\":\"11\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('261', '1010001', '2019-12-21 21:17:10', '{\"humi\":\"12\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('262', '1010001', '2019-12-21 21:17:15', '{\"humi\":\"17\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('263', '1010001', '2019-12-21 21:17:20', '{\"humi\":\"18\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('264', '1010001', '2019-12-21 21:17:25', '{\"humi\":\"10\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('265', '1010001', '2019-12-21 21:17:30', '{\"humi\":\"14\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('266', '1010001', '2019-12-21 21:17:35', '{\"humi\":\"10\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('267', '1010001', '2019-12-21 21:17:40', '{\"humi\":\"11\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('268', '1010001', '2019-12-21 21:17:45', '{\"humi\":\"14\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('269', '1010001', '2019-12-21 21:17:50', '{\"humi\":\"14\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('270', '1010001', '2019-12-21 21:17:55', '{\"humi\":\"17\",\"temp\":\"20\"}', null);
INSERT INTO `thistorydata` VALUES ('271', '1010001', '2019-12-21 21:18:00', '{\"humi\":\"14\",\"temp\":\"20\"}', null);
INSERT INTO `thistorydata` VALUES ('272', '1010001', '2019-12-21 21:18:05', '{\"humi\":\"18\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('273', '1010001', '2019-12-21 21:18:10', '{\"humi\":\"12\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('274', '1010001', '2019-12-21 21:18:15', '{\"humi\":\"16\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('275', '1010001', '2019-12-21 21:18:20', '{\"humi\":\"10\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('276', '1010001', '2019-12-21 21:18:25', '{\"humi\":\"14\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('277', '1010001', '2019-12-21 21:18:30', '{\"humi\":\"17\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('278', '1010001', '2019-12-21 21:18:35', '{\"humi\":\"18\",\"temp\":\"19\"}', null);
INSERT INTO `thistorydata` VALUES ('279', '1010001', '2019-12-21 21:18:40', '{\"humi\":\"14\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('280', '1010001', '2019-12-21 21:18:45', '{\"humi\":\"13\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('281', '1010001', '2019-12-21 21:18:50', '{\"humi\":\"16\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('282', '1010001', '2019-12-21 21:18:55', '{\"humi\":\"18\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('283', '1010001', '2019-12-21 21:19:00', '{\"humi\":\"17\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('284', '1010001', '2019-12-21 21:19:05', '{\"humi\":\"17\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('285', '1010001', '2019-12-21 21:19:10', '{\"humi\":\"13\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('286', '1010001', '2019-12-21 21:19:15', '{\"humi\":\"16\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('287', '1010001', '2019-12-21 21:19:20', '{\"humi\":\"11\",\"temp\":\"19\"}', null);
INSERT INTO `thistorydata` VALUES ('288', '1010001', '2019-12-21 21:19:25', '{\"humi\":\"10\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('289', '1010001', '2019-12-21 21:19:30', '{\"humi\":\"13\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('290', '1010001', '2019-12-21 21:19:35', '{\"humi\":\"13\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('291', '1010001', '2019-12-21 21:19:40', '{\"humi\":\"20\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('292', '1010001', '2019-12-21 21:19:45', '{\"humi\":\"17\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('293', '1010001', '2019-12-21 21:19:50', '{\"humi\":\"13\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('294', '1010001', '2019-12-21 21:19:55', '{\"humi\":\"16\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('295', '1010001', '2019-12-21 21:20:00', '{\"humi\":\"20\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('296', '1010001', '2019-12-21 21:20:05', '{\"humi\":\"19\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('297', '1010001', '2019-12-21 21:20:10', '{\"humi\":\"17\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('298', '1010001', '2019-12-21 21:20:15', '{\"humi\":\"16\",\"temp\":\"20\"}', null);
INSERT INTO `thistorydata` VALUES ('299', '1010001', '2019-12-21 21:20:20', '{\"humi\":\"12\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('300', '1010001', '2019-12-21 21:20:25', '{\"humi\":\"13\",\"temp\":\"19\"}', null);
INSERT INTO `thistorydata` VALUES ('301', '1010001', '2019-12-21 21:20:30', '{\"humi\":\"10\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('302', '1010001', '2019-12-21 21:20:35', '{\"humi\":\"19\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('303', '1010001', '2019-12-21 21:20:40', '{\"humi\":\"11\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('304', '1010001', '2019-12-21 21:20:45', '{\"humi\":\"14\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('305', '1010001', '2019-12-21 21:20:50', '{\"humi\":\"11\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('306', '1010001', '2019-12-21 21:20:55', '{\"humi\":\"17\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('307', '1010001', '2019-12-21 21:21:00', '{\"humi\":\"13\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('308', '1010001', '2019-12-21 21:21:05', '{\"humi\":\"19\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('309', '1010001', '2019-12-21 21:21:10', '{\"humi\":\"17\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('310', '1010001', '2019-12-21 21:21:15', '{\"humi\":\"13\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('311', '1010001', '2019-12-21 21:21:20', '{\"humi\":\"13\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('312', '1010001', '2019-12-21 21:21:25', '{\"humi\":\"12\",\"temp\":\"20\"}', null);
INSERT INTO `thistorydata` VALUES ('313', '1010001', '2019-12-21 21:21:30', '{\"humi\":\"16\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('314', '1010001', '2019-12-21 21:21:35', '{\"humi\":\"10\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('315', '1010001', '2019-12-21 21:21:40', '{\"humi\":\"10\",\"temp\":\"20\"}', null);
INSERT INTO `thistorydata` VALUES ('316', '1010001', '2019-12-21 21:21:45', '{\"humi\":\"11\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('317', '1010001', '2019-12-21 21:21:50', '{\"humi\":\"11\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('318', '1010001', '2019-12-21 21:21:55', '{\"humi\":\"15\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('319', '1010001', '2019-12-21 21:22:00', '{\"humi\":\"15\",\"temp\":\"19\"}', null);
INSERT INTO `thistorydata` VALUES ('320', '1010001', '2019-12-21 21:22:05', '{\"humi\":\"15\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('321', '1010001', '2019-12-21 21:22:10', '{\"humi\":\"13\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('322', '1010001', '2019-12-21 21:22:15', '{\"humi\":\"14\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('323', '1010001', '2019-12-21 21:22:20', '{\"humi\":\"14\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('324', '1010001', '2019-12-21 21:22:25', '{\"humi\":\"10\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('325', '1010001', '2019-12-21 21:22:30', '{\"humi\":\"12\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('326', '1010001', '2019-12-21 21:22:35', '{\"humi\":\"12\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('327', '1010001', '2019-12-21 21:22:40', '{\"humi\":\"12\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('328', '1010001', '2019-12-21 21:22:45', '{\"humi\":\"10\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('329', '1010001', '2019-12-21 21:22:50', '{\"humi\":\"14\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('330', '1010001', '2019-12-21 21:22:55', '{\"humi\":\"16\",\"temp\":\"20\"}', null);
INSERT INTO `thistorydata` VALUES ('331', '1010001', '2019-12-21 21:23:00', '{\"humi\":\"19\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('332', '1010001', '2019-12-21 21:23:05', '{\"humi\":\"15\",\"temp\":\"19\"}', null);
INSERT INTO `thistorydata` VALUES ('333', '1010001', '2019-12-21 21:23:10', '{\"humi\":\"11\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('334', '1010001', '2019-12-21 21:23:15', '{\"humi\":\"14\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('335', '1010001', '2019-12-21 21:23:20', '{\"humi\":\"19\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('336', '1010001', '2019-12-21 21:23:25', '{\"humi\":\"14\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('337', '1010001', '2019-12-21 21:23:30', '{\"humi\":\"17\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('338', '1010001', '2019-12-21 21:23:35', '{\"humi\":\"18\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('339', '1010001', '2019-12-21 21:23:40', '{\"humi\":\"14\",\"temp\":\"19\"}', null);
INSERT INTO `thistorydata` VALUES ('340', '1010001', '2019-12-21 21:23:45', '{\"humi\":\"14\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('341', '1010001', '2019-12-21 21:23:50', '{\"humi\":\"16\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('342', '1010001', '2019-12-21 21:23:55', '{\"humi\":\"12\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('343', '1010001', '2019-12-21 21:24:00', '{\"humi\":\"15\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('344', '1010001', '2019-12-21 21:24:05', '{\"humi\":\"15\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('345', '1010001', '2019-12-21 21:24:10', '{\"humi\":\"14\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('346', '1010001', '2019-12-21 21:24:15', '{\"humi\":\"14\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('347', '1010001', '2019-12-21 21:24:20', '{\"humi\":\"18\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('348', '1010001', '2019-12-21 21:24:25', '{\"humi\":\"18\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('349', '1010001', '2019-12-21 21:24:30', '{\"humi\":\"15\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('350', '1010001', '2019-12-21 21:24:35', '{\"humi\":\"20\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('351', '1010001', '2019-12-21 21:24:40', '{\"humi\":\"13\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('352', '1010001', '2019-12-21 21:24:45', '{\"humi\":\"17\",\"temp\":\"19\"}', null);
INSERT INTO `thistorydata` VALUES ('353', '1010001', '2019-12-21 21:24:50', '{\"humi\":\"19\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('354', '1010001', '2019-12-21 21:24:55', '{\"humi\":\"13\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('355', '1010001', '2019-12-21 21:25:00', '{\"humi\":\"10\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('356', '1010001', '2019-12-21 21:25:05', '{\"humi\":\"15\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('357', '1010001', '2019-12-21 21:25:10', '{\"humi\":\"13\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('358', '1010001', '2019-12-21 21:25:15', '{\"humi\":\"15\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('359', '1010001', '2019-12-21 21:25:20', '{\"humi\":\"16\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('360', '1010001', '2019-12-21 21:25:25', '{\"humi\":\"11\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('361', '1010001', '2019-12-21 21:25:30', '{\"humi\":\"10\",\"temp\":\"19\"}', null);
INSERT INTO `thistorydata` VALUES ('362', '1010001', '2019-12-21 21:25:35', '{\"humi\":\"17\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('363', '1010001', '2019-12-21 21:25:40', '{\"humi\":\"15\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('364', '1010001', '2019-12-21 21:25:45', '{\"humi\":\"13\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('365', '1010001', '2019-12-21 21:25:50', '{\"humi\":\"19\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('366', '1010001', '2019-12-21 21:25:55', '{\"humi\":\"16\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('367', '1010001', '2019-12-21 21:26:00', '{\"humi\":\"17\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('368', '1010001', '2019-12-21 21:26:05', '{\"humi\":\"12\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('369', '1010001', '2019-12-21 21:26:10', '{\"humi\":\"15\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('370', '1010001', '2019-12-21 21:26:15', '{\"humi\":\"16\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('371', '1010001', '2019-12-21 21:26:20', '{\"humi\":\"13\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('372', '1010001', '2019-12-21 21:26:25', '{\"humi\":\"19\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('373', '1010001', '2019-12-21 21:26:30', '{\"humi\":\"10\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('374', '1010001', '2019-12-21 21:26:35', '{\"humi\":\"19\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('375', '1010001', '2019-12-21 21:26:40', '{\"humi\":\"20\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('376', '1010001', '2019-12-21 21:26:45', '{\"humi\":\"17\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('377', '1010001', '2019-12-21 21:26:50', '{\"humi\":\"15\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('378', '1010001', '2019-12-21 21:26:55', '{\"humi\":\"12\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('379', '1010001', '2019-12-21 21:27:00', '{\"humi\":\"16\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('380', '1010001', '2019-12-21 21:27:05', '{\"humi\":\"19\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('381', '1010001', '2019-12-21 21:27:10', '{\"humi\":\"12\",\"temp\":\"19\"}', null);
INSERT INTO `thistorydata` VALUES ('382', '1010001', '2019-12-21 21:27:15', '{\"humi\":\"18\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('383', '1010001', '2019-12-21 21:27:20', '{\"humi\":\"10\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('384', '1010001', '2019-12-21 21:27:25', '{\"humi\":\"16\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('385', '1010001', '2019-12-21 21:27:30', '{\"humi\":\"17\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('386', '1010001', '2019-12-21 21:27:35', '{\"humi\":\"15\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('387', '1010001', '2019-12-21 21:27:40', '{\"humi\":\"17\",\"temp\":\"20\"}', null);
INSERT INTO `thistorydata` VALUES ('388', '1010001', '2019-12-21 21:27:45', '{\"humi\":\"16\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('389', '1010001', '2019-12-21 21:27:50', '{\"humi\":\"14\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('390', '1010001', '2019-12-21 21:27:55', '{\"humi\":\"14\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('391', '1010001', '2019-12-21 21:28:00', '{\"humi\":\"13\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('392', '1010001', '2019-12-21 21:28:05', '{\"humi\":\"18\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('393', '1010001', '2019-12-21 21:28:10', '{\"humi\":\"16\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('394', '1010001', '2019-12-21 21:28:15', '{\"humi\":\"11\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('395', '1010001', '2019-12-21 21:28:20', '{\"humi\":\"10\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('396', '1010001', '2019-12-21 21:28:25', '{\"humi\":\"12\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('397', '1010001', '2019-12-21 21:28:30', '{\"humi\":\"15\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('398', '1010001', '2019-12-21 21:28:35', '{\"humi\":\"13\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('399', '1010001', '2019-12-21 21:28:40', '{\"humi\":\"18\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('400', '1010001', '2019-12-21 21:28:45', '{\"humi\":\"12\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('401', '1010001', '2019-12-21 21:28:50', '{\"humi\":\"13\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('402', '1010001', '2019-12-21 21:28:55', '{\"humi\":\"12\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('403', '1010001', '2019-12-21 21:29:00', '{\"humi\":\"18\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('404', '1010001', '2019-12-21 21:29:05', '{\"humi\":\"18\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('405', '1010001', '2019-12-21 21:29:10', '{\"humi\":\"12\",\"temp\":\"20\"}', null);
INSERT INTO `thistorydata` VALUES ('406', '1010001', '2019-12-21 21:29:15', '{\"humi\":\"17\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('407', '1010001', '2019-12-21 21:29:20', '{\"humi\":\"13\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('408', '1010001', '2019-12-21 21:29:25', '{\"humi\":\"16\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('409', '1010001', '2019-12-21 21:29:30', '{\"humi\":\"16\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('410', '1010001', '2019-12-21 21:29:35', '{\"humi\":\"11\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('411', '1010001', '2019-12-21 21:29:40', '{\"humi\":\"14\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('412', '1010001', '2019-12-21 21:29:45', '{\"humi\":\"11\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('413', '1010001', '2019-12-21 21:29:50', '{\"humi\":\"10\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('414', '1010001', '2019-12-21 21:29:55', '{\"humi\":\"17\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('415', '1010001', '2019-12-21 21:30:00', '{\"humi\":\"12\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('416', '1010001', '2019-12-21 21:30:05', '{\"humi\":\"19\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('417', '1010001', '2019-12-21 21:30:10', '{\"humi\":\"16\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('418', '1010001', '2019-12-21 21:30:15', '{\"humi\":\"15\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('419', '1010001', '2019-12-21 21:30:20', '{\"humi\":\"18\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('420', '1010001', '2019-12-21 21:30:25', '{\"humi\":\"20\",\"temp\":\"20\"}', null);
INSERT INTO `thistorydata` VALUES ('421', '1010001', '2019-12-21 21:30:30', '{\"humi\":\"18\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('422', '1010001', '2019-12-21 21:30:35', '{\"humi\":\"15\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('423', '1010001', '2019-12-21 21:30:40', '{\"humi\":\"17\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('424', '1010001', '2019-12-21 21:30:45', '{\"humi\":\"15\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('425', '1010001', '2019-12-21 21:30:50', '{\"humi\":\"13\",\"temp\":\"20\"}', null);
INSERT INTO `thistorydata` VALUES ('426', '1010001', '2019-12-21 21:30:55', '{\"humi\":\"15\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('427', '1010001', '2019-12-21 21:31:00', '{\"humi\":\"11\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('428', '1010001', '2019-12-21 21:31:05', '{\"humi\":\"13\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('429', '1010001', '2019-12-21 21:31:10', '{\"humi\":\"17\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('430', '1010001', '2019-12-21 21:31:15', '{\"humi\":\"11\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('431', '1010001', '2019-12-21 21:31:20', '{\"humi\":\"17\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('432', '1010001', '2019-12-21 21:31:25', '{\"humi\":\"13\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('433', '1010001', '2019-12-21 21:31:30', '{\"humi\":\"11\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('434', '1010001', '2019-12-21 21:31:35', '{\"humi\":\"16\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('435', '1010001', '2019-12-21 21:31:40', '{\"humi\":\"13\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('436', '1010001', '2019-12-21 21:31:45', '{\"humi\":\"10\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('437', '1010001', '2019-12-21 21:31:50', '{\"humi\":\"16\",\"temp\":\"19\"}', null);
INSERT INTO `thistorydata` VALUES ('438', '1010001', '2019-12-21 21:31:55', '{\"humi\":\"19\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('439', '1010001', '2019-12-21 21:32:00', '{\"humi\":\"18\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('440', '1010001', '2019-12-21 21:32:05', '{\"humi\":\"17\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('441', '1010001', '2019-12-21 21:32:10', '{\"humi\":\"17\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('442', '1010001', '2019-12-21 21:32:15', '{\"humi\":\"18\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('443', '1010001', '2019-12-21 21:32:20', '{\"humi\":\"17\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('444', '1010001', '2019-12-21 21:32:25', '{\"humi\":\"15\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('445', '1010001', '2019-12-21 21:32:30', '{\"humi\":\"18\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('446', '1010001', '2019-12-21 21:32:35', '{\"humi\":\"14\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('447', '1010001', '2019-12-21 21:32:40', '{\"humi\":\"11\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('448', '1010001', '2019-12-21 21:32:45', '{\"humi\":\"15\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('449', '1010001', '2019-12-21 21:32:50', '{\"humi\":\"18\",\"temp\":\"19\"}', null);
INSERT INTO `thistorydata` VALUES ('450', '1010001', '2019-12-21 21:32:55', '{\"humi\":\"16\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('451', '1010001', '2019-12-21 21:33:00', '{\"humi\":\"11\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('452', '1010001', '2019-12-21 21:33:05', '{\"humi\":\"10\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('453', '1010001', '2019-12-21 21:33:10', '{\"humi\":\"11\",\"temp\":\"20\"}', null);
INSERT INTO `thistorydata` VALUES ('454', '1010001', '2019-12-21 21:33:15', '{\"humi\":\"13\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('455', '1010001', '2019-12-21 21:33:20', '{\"humi\":\"15\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('456', '1010001', '2019-12-21 21:33:25', '{\"humi\":\"19\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('457', '1010001', '2019-12-21 21:33:30', '{\"humi\":\"12\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('458', '1010001', '2019-12-21 21:33:35', '{\"humi\":\"12\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('459', '1010001', '2019-12-21 21:33:40', '{\"humi\":\"17\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('460', '1010001', '2019-12-21 21:33:45', '{\"humi\":\"14\",\"temp\":\"20\"}', null);
INSERT INTO `thistorydata` VALUES ('461', '1010001', '2019-12-21 21:33:50', '{\"humi\":\"20\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('462', '1010001', '2019-12-21 21:33:55', '{\"humi\":\"11\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('463', '1010001', '2019-12-21 21:34:00', '{\"humi\":\"16\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('464', '1010001', '2019-12-21 21:34:05', '{\"humi\":\"20\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('465', '1010001', '2019-12-21 21:34:10', '{\"humi\":\"13\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('466', '1010001', '2019-12-21 21:34:15', '{\"humi\":\"20\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('467', '1010001', '2019-12-21 21:34:20', '{\"humi\":\"16\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('468', '1010001', '2019-12-21 21:34:25', '{\"humi\":\"11\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('469', '1010001', '2019-12-21 21:34:30', '{\"humi\":\"16\",\"temp\":\"20\"}', null);
INSERT INTO `thistorydata` VALUES ('470', '1010001', '2019-12-21 21:34:35', '{\"humi\":\"17\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('471', '1010001', '2019-12-21 21:34:40', '{\"humi\":\"20\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('472', '1010001', '2019-12-21 21:34:45', '{\"humi\":\"16\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('473', '1010001', '2019-12-21 21:34:50', '{\"humi\":\"10\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('474', '1010001', '2019-12-21 21:34:55', '{\"humi\":\"11\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('475', '1010001', '2019-12-21 21:35:00', '{\"humi\":\"12\",\"temp\":\"20\"}', null);
INSERT INTO `thistorydata` VALUES ('476', '1010001', '2019-12-21 21:35:05', '{\"humi\":\"12\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('477', '1010001', '2019-12-21 21:35:10', '{\"humi\":\"11\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('478', '1010001', '2019-12-21 21:35:15', '{\"humi\":\"14\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('479', '1010001', '2019-12-21 21:35:20', '{\"humi\":\"18\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('480', '1010001', '2019-12-21 21:35:25', '{\"humi\":\"10\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('481', '1010001', '2019-12-21 21:35:30', '{\"humi\":\"11\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('482', '1010001', '2019-12-21 21:35:35', '{\"humi\":\"10\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('483', '1010001', '2019-12-21 21:35:40', '{\"humi\":\"13\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('484', '1010001', '2019-12-21 21:35:45', '{\"humi\":\"15\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('485', '1010001', '2019-12-21 21:35:50', '{\"humi\":\"10\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('486', '1010001', '2019-12-21 21:35:55', '{\"humi\":\"19\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('487', '1010001', '2019-12-21 21:36:00', '{\"humi\":\"14\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('488', '1010001', '2019-12-21 21:36:05', '{\"humi\":\"19\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('489', '1010001', '2019-12-21 21:36:10', '{\"humi\":\"19\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('490', '1010001', '2019-12-21 21:36:15', '{\"humi\":\"15\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('491', '1010001', '2019-12-21 21:36:20', '{\"humi\":\"18\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('492', '1010001', '2019-12-21 21:36:25', '{\"humi\":\"14\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('493', '1010001', '2019-12-21 21:36:30', '{\"humi\":\"12\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('494', '1010001', '2019-12-21 21:36:35', '{\"humi\":\"16\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('495', '1010001', '2019-12-21 21:36:40', '{\"humi\":\"15\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('496', '1010001', '2019-12-21 21:36:45', '{\"humi\":\"14\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('497', '1010001', '2019-12-21 21:36:50', '{\"humi\":\"12\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('498', '1010001', '2019-12-21 21:36:55', '{\"humi\":\"12\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('499', '1010001', '2019-12-21 21:37:00', '{\"humi\":\"13\",\"temp\":\"19\"}', null);
INSERT INTO `thistorydata` VALUES ('500', '1010001', '2019-12-21 21:37:05', '{\"humi\":\"18\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('501', '1010001', '2019-12-21 21:37:10', '{\"humi\":\"19\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('502', '1010001', '2019-12-21 21:37:15', '{\"humi\":\"15\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('503', '1010001', '2019-12-21 21:37:20', '{\"humi\":\"16\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('504', '1010001', '2019-12-21 21:37:25', '{\"humi\":\"15\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('505', '1010001', '2019-12-21 21:37:30', '{\"humi\":\"11\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('506', '1010001', '2019-12-21 21:37:35', '{\"humi\":\"13\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('507', '1010001', '2019-12-21 21:37:40', '{\"humi\":\"20\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('508', '1010001', '2019-12-21 21:37:45', '{\"humi\":\"17\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('509', '1010001', '2019-12-21 21:37:50', '{\"humi\":\"13\",\"temp\":\"20\"}', null);
INSERT INTO `thistorydata` VALUES ('510', '1010001', '2019-12-21 21:37:55', '{\"humi\":\"13\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('511', '1010001', '2019-12-21 21:38:00', '{\"humi\":\"18\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('512', '1010001', '2019-12-21 21:38:05', '{\"humi\":\"19\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('513', '1010001', '2019-12-21 21:38:10', '{\"humi\":\"14\",\"temp\":\"20\"}', null);
INSERT INTO `thistorydata` VALUES ('514', '1010001', '2019-12-21 21:38:15', '{\"humi\":\"19\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('515', '1010001', '2019-12-21 21:38:20', '{\"humi\":\"14\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('516', '1010001', '2019-12-21 21:38:25', '{\"humi\":\"10\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('517', '1010001', '2019-12-21 21:38:30', '{\"humi\":\"10\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('518', '1010001', '2019-12-21 21:38:35', '{\"humi\":\"13\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('519', '1010001', '2019-12-21 21:38:40', '{\"humi\":\"18\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('520', '1010001', '2019-12-21 21:38:45', '{\"humi\":\"16\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('521', '1010001', '2019-12-21 21:38:50', '{\"humi\":\"12\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('522', '1010001', '2019-12-21 21:38:55', '{\"humi\":\"14\",\"temp\":\"20\"}', null);
INSERT INTO `thistorydata` VALUES ('523', '1010001', '2019-12-21 21:39:00', '{\"humi\":\"16\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('524', '1010001', '2019-12-21 21:39:05', '{\"humi\":\"16\",\"temp\":\"19\"}', null);
INSERT INTO `thistorydata` VALUES ('525', '1010001', '2019-12-21 21:39:10', '{\"humi\":\"13\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('526', '1010001', '2019-12-21 21:39:15', '{\"humi\":\"19\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('527', '1010001', '2019-12-21 21:39:20', '{\"humi\":\"15\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('528', '1010001', '2019-12-21 21:39:25', '{\"humi\":\"12\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('529', '1010001', '2019-12-21 21:39:30', '{\"humi\":\"17\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('530', '1010001', '2019-12-21 21:39:35', '{\"humi\":\"12\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('531', '1010001', '2019-12-21 21:39:40', '{\"humi\":\"12\",\"temp\":\"19\"}', null);
INSERT INTO `thistorydata` VALUES ('532', '1010001', '2019-12-21 21:39:45', '{\"humi\":\"15\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('533', '1010001', '2019-12-21 21:39:50', '{\"humi\":\"18\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('534', '1010001', '2019-12-21 21:39:55', '{\"humi\":\"12\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('535', '1010001', '2019-12-21 21:40:00', '{\"humi\":\"11\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('536', '1010001', '2019-12-21 21:40:05', '{\"humi\":\"13\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('537', '1010001', '2019-12-21 21:40:10', '{\"humi\":\"16\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('538', '1010001', '2019-12-21 21:40:15', '{\"humi\":\"15\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('539', '1010001', '2019-12-21 21:40:20', '{\"humi\":\"10\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('540', '1010001', '2019-12-21 21:40:25', '{\"humi\":\"10\",\"temp\":\"19\"}', null);
INSERT INTO `thistorydata` VALUES ('541', '1010001', '2019-12-21 21:40:30', '{\"humi\":\"13\",\"temp\":\"19\"}', null);
INSERT INTO `thistorydata` VALUES ('542', '1010001', '2019-12-21 21:40:35', '{\"humi\":\"14\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('543', '1010001', '2019-12-21 21:40:40', '{\"humi\":\"11\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('544', '1010001', '2019-12-21 21:40:45', '{\"humi\":\"18\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('545', '1010001', '2019-12-21 21:40:50', '{\"humi\":\"16\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('546', '1010001', '2019-12-21 21:40:55', '{\"humi\":\"17\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('547', '1010001', '2019-12-21 21:41:00', '{\"humi\":\"17\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('548', '1010001', '2019-12-21 21:41:05', '{\"humi\":\"17\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('549', '1010001', '2019-12-21 21:41:10', '{\"humi\":\"16\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('550', '1010001', '2019-12-21 21:41:15', '{\"humi\":\"17\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('551', '1010001', '2019-12-21 21:41:20', '{\"humi\":\"17\",\"temp\":\"19\"}', null);
INSERT INTO `thistorydata` VALUES ('552', '1010001', '2019-12-21 21:41:25', '{\"humi\":\"10\",\"temp\":\"19\"}', null);
INSERT INTO `thistorydata` VALUES ('553', '1010001', '2019-12-21 21:41:30', '{\"humi\":\"15\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('554', '1010001', '2019-12-21 21:41:35', '{\"humi\":\"17\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('555', '1010001', '2019-12-21 21:41:40', '{\"humi\":\"12\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('556', '1010001', '2019-12-21 21:41:45', '{\"humi\":\"17\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('557', '1010001', '2019-12-21 21:41:50', '{\"humi\":\"18\",\"temp\":\"19\"}', null);
INSERT INTO `thistorydata` VALUES ('558', '1010001', '2019-12-21 21:41:55', '{\"humi\":\"16\",\"temp\":\"20\"}', null);
INSERT INTO `thistorydata` VALUES ('559', '1010001', '2019-12-21 21:42:00', '{\"humi\":\"13\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('560', '1010001', '2019-12-21 21:42:05', '{\"humi\":\"15\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('561', '1010001', '2019-12-21 21:42:10', '{\"humi\":\"16\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('562', '1010001', '2019-12-21 21:42:15', '{\"humi\":\"18\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('563', '1010001', '2019-12-21 21:42:20', '{\"humi\":\"18\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('564', '1010001', '2019-12-21 21:42:25', '{\"humi\":\"10\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('565', '1010001', '2019-12-21 21:42:30', '{\"humi\":\"15\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('566', '1010001', '2019-12-21 21:42:35', '{\"humi\":\"20\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('567', '1010001', '2019-12-21 21:42:40', '{\"humi\":\"14\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('568', '1010001', '2019-12-21 21:42:45', '{\"humi\":\"16\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('569', '1010001', '2019-12-21 21:42:50', '{\"humi\":\"10\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('570', '1010001', '2019-12-21 21:42:55', '{\"humi\":\"16\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('571', '1010001', '2019-12-21 21:43:00', '{\"humi\":\"10\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('572', '1010001', '2019-12-21 21:43:05', '{\"humi\":\"17\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('573', '1010001', '2019-12-21 21:43:10', '{\"humi\":\"13\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('574', '1010001', '2019-12-21 21:43:15', '{\"humi\":\"14\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('575', '1010001', '2019-12-21 21:43:20', '{\"humi\":\"12\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('576', '1010001', '2019-12-21 21:43:25', '{\"humi\":\"12\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('577', '1010001', '2019-12-21 21:43:30', '{\"humi\":\"17\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('578', '1010001', '2019-12-21 21:43:35', '{\"humi\":\"10\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('579', '1010001', '2019-12-21 21:43:40', '{\"humi\":\"14\",\"temp\":\"20\"}', null);
INSERT INTO `thistorydata` VALUES ('580', '1010001', '2019-12-21 21:43:45', '{\"humi\":\"11\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('581', '1010001', '2019-12-21 21:43:50', '{\"humi\":\"11\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('582', '1010001', '2019-12-21 21:43:55', '{\"humi\":\"13\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('583', '1010001', '2019-12-21 21:44:00', '{\"humi\":\"10\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('584', '1010001', '2019-12-21 21:44:05', '{\"humi\":\"14\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('585', '1010001', '2019-12-21 21:44:10', '{\"humi\":\"13\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('586', '1010001', '2019-12-21 21:44:15', '{\"humi\":\"17\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('587', '1010001', '2019-12-21 21:44:20', '{\"humi\":\"19\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('588', '1010001', '2019-12-21 21:44:25', '{\"humi\":\"13\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('589', '1010001', '2019-12-21 21:44:30', '{\"humi\":\"12\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('590', '1010001', '2019-12-21 21:44:35', '{\"humi\":\"18\",\"temp\":\"20\"}', null);
INSERT INTO `thistorydata` VALUES ('591', '1010001', '2019-12-21 21:44:40', '{\"humi\":\"10\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('592', '1010001', '2019-12-21 21:44:45', '{\"humi\":\"18\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('593', '1010001', '2019-12-21 21:44:50', '{\"humi\":\"17\",\"temp\":\"19\"}', null);
INSERT INTO `thistorydata` VALUES ('594', '1010001', '2019-12-21 21:44:55', '{\"humi\":\"14\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('595', '1010001', '2019-12-21 21:45:00', '{\"humi\":\"16\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('596', '1010001', '2019-12-21 21:45:05', '{\"humi\":\"17\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('597', '1010001', '2019-12-21 21:45:10', '{\"humi\":\"13\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('598', '1010001', '2019-12-21 21:45:15', '{\"humi\":\"12\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('599', '1010001', '2019-12-21 21:45:20', '{\"humi\":\"16\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('600', '1010001', '2019-12-21 21:45:25', '{\"humi\":\"13\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('601', '1010001', '2019-12-21 21:45:30', '{\"humi\":\"16\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('602', '1010001', '2019-12-21 21:45:35', '{\"humi\":\"12\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('603', '1010001', '2019-12-21 21:45:40', '{\"humi\":\"12\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('604', '1010001', '2019-12-21 21:45:45', '{\"humi\":\"16\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('605', '1010001', '2019-12-21 21:45:50', '{\"humi\":\"15\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('606', '1010001', '2019-12-21 21:45:55', '{\"humi\":\"17\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('607', '1010001', '2019-12-21 21:46:00', '{\"humi\":\"10\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('608', '1010001', '2019-12-21 21:46:05', '{\"humi\":\"17\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('609', '1010001', '2019-12-21 21:46:10', '{\"humi\":\"15\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('610', '1010001', '2019-12-21 21:46:15', '{\"humi\":\"18\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('611', '1010001', '2019-12-21 21:46:20', '{\"humi\":\"10\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('612', '1010001', '2019-12-21 21:46:25', '{\"humi\":\"17\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('613', '1010001', '2019-12-21 21:46:30', '{\"humi\":\"14\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('614', '1010001', '2019-12-21 21:46:35', '{\"humi\":\"13\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('615', '1010001', '2019-12-21 21:46:40', '{\"humi\":\"11\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('616', '1010001', '2019-12-21 21:46:45', '{\"humi\":\"16\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('617', '1010001', '2019-12-21 21:46:50', '{\"humi\":\"10\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('618', '1010001', '2019-12-21 21:46:55', '{\"humi\":\"18\",\"temp\":\"19\"}', null);
INSERT INTO `thistorydata` VALUES ('619', '1010001', '2019-12-21 21:47:00', '{\"humi\":\"16\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('620', '1010001', '2019-12-21 21:47:05', '{\"humi\":\"13\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('621', '1010001', '2019-12-21 21:47:10', '{\"humi\":\"17\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('622', '1010001', '2019-12-21 21:47:15', '{\"humi\":\"14\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('623', '1010001', '2019-12-21 21:47:20', '{\"humi\":\"11\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('624', '1010001', '2019-12-21 21:47:25', '{\"humi\":\"12\",\"temp\":\"15\"}', null);
INSERT INTO `thistorydata` VALUES ('625', '1010001', '2019-12-21 21:47:30', '{\"humi\":\"17\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('626', '1010001', '2019-12-21 21:47:35', '{\"humi\":\"12\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('627', '1010001', '2019-12-21 21:47:40', '{\"humi\":\"10\",\"temp\":\"19\"}', null);
INSERT INTO `thistorydata` VALUES ('628', '1010001', '2019-12-21 21:47:45', '{\"humi\":\"15\",\"temp\":\"12\"}', null);
INSERT INTO `thistorydata` VALUES ('629', '1010001', '2019-12-21 21:47:50', '{\"humi\":\"13\",\"temp\":\"20\"}', null);
INSERT INTO `thistorydata` VALUES ('630', '1010001', '2019-12-21 21:47:55', '{\"humi\":\"17\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('631', '1010001', '2019-12-21 21:48:00', '{\"humi\":\"20\",\"temp\":\"20\"}', null);
INSERT INTO `thistorydata` VALUES ('632', '1010001', '2019-12-21 21:48:05', '{\"humi\":\"12\",\"temp\":\"11\"}', null);
INSERT INTO `thistorydata` VALUES ('633', '1010001', '2019-12-21 21:48:10', '{\"humi\":\"11\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('634', '1010001', '2019-12-21 21:48:15', '{\"humi\":\"11\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('635', '1010001', '2019-12-21 21:48:20', '{\"humi\":\"11\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('636', '1010001', '2019-12-21 21:48:25', '{\"humi\":\"11\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('637', '1010001', '2019-12-21 21:48:30', '{\"humi\":\"16\",\"temp\":\"19\"}', null);
INSERT INTO `thistorydata` VALUES ('638', '1010001', '2019-12-21 21:48:35', '{\"humi\":\"19\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('639', '1010001', '2019-12-21 21:48:40', '{\"humi\":\"12\",\"temp\":\"19\"}', null);
INSERT INTO `thistorydata` VALUES ('640', '1010001', '2019-12-21 21:48:45', '{\"humi\":\"13\",\"temp\":\"13\"}', null);
INSERT INTO `thistorydata` VALUES ('641', '1010001', '2019-12-21 21:48:50', '{\"humi\":\"13\",\"temp\":\"19\"}', null);
INSERT INTO `thistorydata` VALUES ('642', '1010001', '2019-12-21 21:48:55', '{\"humi\":\"17\",\"temp\":\"17\"}', null);
INSERT INTO `thistorydata` VALUES ('643', '1010001', '2019-12-21 21:49:00', '{\"humi\":\"10\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('644', '1010001', '2019-12-21 21:49:05', '{\"humi\":\"13\",\"temp\":\"14\"}', null);
INSERT INTO `thistorydata` VALUES ('645', '1010001', '2019-12-21 21:49:10', '{\"humi\":\"11\",\"temp\":\"20\"}', null);
INSERT INTO `thistorydata` VALUES ('646', '1010001', '2019-12-21 21:49:15', '{\"humi\":\"14\",\"temp\":\"10\"}', null);
INSERT INTO `thistorydata` VALUES ('647', '1010001', '2019-12-21 21:49:20', '{\"humi\":\"12\",\"temp\":\"16\"}', null);
INSERT INTO `thistorydata` VALUES ('648', '1010001', '2019-12-21 21:49:25', '{\"humi\":\"13\",\"temp\":\"18\"}', null);
INSERT INTO `thistorydata` VALUES ('649', null, '2021-01-03 01:01:57', '{\"power\":\"on\",\"color\":\"r\"}', '');
INSERT INTO `thistorydata` VALUES ('650', null, '2021-01-03 01:01:59', '{\"power\":\"on\",\"color\":\"g\"}', '');
INSERT INTO `thistorydata` VALUES ('651', null, '2021-01-03 01:02:00', '{\"power\":\"on\",\"color\":\"b\"}', '');
INSERT INTO `thistorydata` VALUES ('652', null, '2021-01-03 02:39:52', '{\"power\":\"off\"}', '');
INSERT INTO `thistorydata` VALUES ('653', null, '2021-01-03 13:05:05', '{\"power\":\"on\",\"color\":\"r\"}', '');
INSERT INTO `thistorydata` VALUES ('654', null, '2021-01-03 13:59:30', '{\"power\":\"off\"}', '');
INSERT INTO `thistorydata` VALUES ('655', null, '2021-01-03 13:59:35', '{\"power\":\"on\",\"color\":\"r\"}', '');
INSERT INTO `thistorydata` VALUES ('656', null, '2021-01-03 15:30:49', '{\"power\":\"off\"}', '');
INSERT INTO `thistorydata` VALUES ('657', null, '2021-01-03 15:30:52', '{\"power\":\"off\"}', '');
INSERT INTO `thistorydata` VALUES ('658', null, '2021-01-03 15:30:54', '{\"power\":\"off\"}', '');
INSERT INTO `thistorydata` VALUES ('659', null, '2021-01-03 15:30:57', '{\"power\":\"off\"}', '');
INSERT INTO `thistorydata` VALUES ('660', null, '2021-01-03 15:30:57', '{\"power\":\"on\",\"color\":\"r\"}', '');
INSERT INTO `thistorydata` VALUES ('661', null, '2021-01-03 15:30:59', '{\"power\":\"off\"}', '');
INSERT INTO `thistorydata` VALUES ('662', null, '2021-01-03 18:13:19', '{\"power\":\"on\",\"color\":\"r\"}', '');
INSERT INTO `thistorydata` VALUES ('663', null, '2021-01-03 18:13:31', '{\"power\":\"on\",\"color\":\"g\"}', '');
INSERT INTO `thistorydata` VALUES ('664', null, '2021-01-03 18:48:20', '{\"power\":\"on\",\"color\":\"r\"}', '');
INSERT INTO `thistorydata` VALUES ('665', null, '2021-01-03 18:49:24', '{\"power\":\"on\",\"color\":\"r\"}', '');
INSERT INTO `thistorydata` VALUES ('666', null, '2021-01-03 18:50:04', '{\"power\":\"on\",\"color\":\"r\"}', '');
INSERT INTO `thistorydata` VALUES ('667', null, '2021-01-03 18:50:05', '{\"power\":\"on\",\"color\":\"g\"}', '');
INSERT INTO `thistorydata` VALUES ('668', null, '2021-01-03 18:50:06', '{\"power\":\"on\",\"color\":\"b\"}', '');
INSERT INTO `thistorydata` VALUES ('669', null, '2021-01-03 18:50:55', '{\"humi\":\"45\",\"temp\":\"31\"}', '');
INSERT INTO `thistorydata` VALUES ('670', null, '2021-01-03 18:51:05', '{\"humi\":\"45\",\"temp\":\"31\"}', '');
INSERT INTO `thistorydata` VALUES ('671', null, '2021-01-03 18:51:10', '{\"humi\":\"45\",\"temp\":\"32\"}', '');
INSERT INTO `thistorydata` VALUES ('672', null, '2021-01-03 19:00:46', '{\"humi\":\"45\",\"temp\":\"32\"}', '');
INSERT INTO `thistorydata` VALUES ('673', null, '2021-01-03 19:00:52', '{\"humi\":\"45\",\"temp\":\"31\"}', '');
INSERT INTO `thistorydata` VALUES ('674', null, '2021-01-03 19:01:10', '{\"humi\":\"45\",\"temp\":\"31\"}', '');
INSERT INTO `thistorydata` VALUES ('675', null, '2021-01-03 19:01:15', '{\"humi\":\"45\",\"temp\":\"32\"}', '');
INSERT INTO `thistorydata` VALUES ('676', null, '2021-01-03 19:01:30', '{\"humi\":\"48\",\"temp\":\"14\"}', '');
INSERT INTO `thistorydata` VALUES ('677', null, '2021-01-03 19:09:26', '{\"humi\":\"45\",\"temp\":\"32\"}', '');
INSERT INTO `thistorydata` VALUES ('678', null, '2021-01-03 19:09:33', '{\"humi\":\"48\",\"temp\":\"14\"}', '');
INSERT INTO `thistorydata` VALUES ('679', null, '2021-01-03 19:09:34', '{\"humi\":\"48\",\"temp\":\"14\"}', '');
INSERT INTO `thistorydata` VALUES ('680', null, '2021-01-03 19:09:36', '{\"humi\":\"48\",\"temp\":\"14\"}', '');
INSERT INTO `thistorydata` VALUES ('681', null, '2021-01-03 19:09:39', '{\"humi\":\"45\",\"temp\":\"32\"}', '');
INSERT INTO `thistorydata` VALUES ('682', null, '2021-01-03 19:12:16', '{\"humi\":\"48\",\"temp\":\"14\"}', '');
INSERT INTO `thistorydata` VALUES ('683', null, '2021-01-03 19:28:52', '{\"humi\":\"48\",\"temp\":\"35\"}', '');
INSERT INTO `thistorydata` VALUES ('684', null, '2021-01-03 19:29:03', '{\"humi\":\"58\",\"temp\":\"33\"}', '');
INSERT INTO `thistorydata` VALUES ('685', null, '2021-01-03 20:18:56', '{\"power\":\"on\",\"color\":\"r\"}', '');
INSERT INTO `thistorydata` VALUES ('686', null, '2021-01-03 22:18:34', '{\"power\":\"off\"}', '');
INSERT INTO `thistorydata` VALUES ('687', null, '2021-01-03 22:18:36', '{\"power\":\"off\"}', '');
INSERT INTO `thistorydata` VALUES ('688', null, '2021-01-03 22:18:38', '{\"power\":\"on\",\"color\":\"r\"}', '');
INSERT INTO `thistorydata` VALUES ('689', null, '2021-01-03 22:18:39', '{\"power\":\"off\"}', '');
INSERT INTO `thistorydata` VALUES ('690', null, '2021-01-03 22:18:47', '{\"power\":\"on\",\"color\":\"r\"}', '');
INSERT INTO `thistorydata` VALUES ('691', null, '2021-01-03 22:18:48', '{\"power\":\"on\",\"color\":\"b\"}', '');
INSERT INTO `thistorydata` VALUES ('692', null, '2021-01-03 22:18:50', '{\"power\":\"on\",\"color\":\"g\"}', '');
INSERT INTO `thistorydata` VALUES ('693', null, '2021-01-03 22:18:50', '{\"power\":\"off\"}', '');
INSERT INTO `thistorydata` VALUES ('694', null, '2021-01-03 22:18:52', '{\"power\":\"on\",\"color\":\"g\"}', '');
INSERT INTO `thistorydata` VALUES ('695', null, '2021-01-03 22:18:53', '{\"power\":\"off\"}', '');
INSERT INTO `thistorydata` VALUES ('696', null, '2021-01-03 22:19:24', '{\"power\":\"on\",\"color\":\"g\"}', '');
INSERT INTO `thistorydata` VALUES ('697', null, '2021-01-03 22:20:08', '{\"power\":\"off\"}', '');
INSERT INTO `thistorydata` VALUES ('698', null, '2021-01-04 00:02:09', '{\"power\":\"off\"}', '');
INSERT INTO `thistorydata` VALUES ('699', null, '2021-01-04 00:02:10', '{\"power\":\"on\",\"color\":\"g\"}', '');
INSERT INTO `thistorydata` VALUES ('700', null, '2021-01-04 00:02:12', '{\"power\":\"on\",\"color\":\"b\"}', '');
INSERT INTO `thistorydata` VALUES ('701', null, '2021-01-04 00:02:13', '{\"power\":\"on\",\"color\":\"r\"}', '');
INSERT INTO `thistorydata` VALUES ('702', null, '2021-01-04 00:03:30', '{\"power\":\"off\"}', '');
INSERT INTO `thistorydata` VALUES ('703', null, '2021-01-04 20:09:54', '{\"power\":\"on\",\"color\":\"g\"}', '');
INSERT INTO `thistorydata` VALUES ('704', null, '2021-01-04 20:09:56', '{\"power\":\"off\"}', '');
INSERT INTO `thistorydata` VALUES ('705', null, '2021-01-04 20:09:59', '{\"power\":\"on\",\"color\":\"g\"}', '');
INSERT INTO `thistorydata` VALUES ('706', null, '2021-01-04 20:18:16', '{\"power\":\"on\",\"color\":\"g\"}', '');
INSERT INTO `thistorydata` VALUES ('707', null, '2021-01-04 20:18:18', '{\"power\":\"on\",\"color\":\"b\"}', '');
INSERT INTO `thistorydata` VALUES ('708', null, '2021-01-04 20:18:20', '{\"power\":\"on\",\"color\":\"r\"}', '');
INSERT INTO `thistorydata` VALUES ('709', null, '2021-01-04 20:18:21', '{\"power\":\"off\"}', '');
INSERT INTO `thistorydata` VALUES ('710', null, '2021-01-05 00:42:04', '{\"power\":\"on\",\"color\":\"r\"}', '');
INSERT INTO `thistorydata` VALUES ('711', null, '2021-01-05 00:42:42', '{\"power\":\"on\",\"color\":\"g\"}', '');
INSERT INTO `thistorydata` VALUES ('712', null, '2021-01-05 00:42:44', '{\"power\":\"on\",\"color\":\"b\"}', '');
INSERT INTO `thistorydata` VALUES ('713', null, '2021-01-05 00:42:45', '{\"power\":\"on\",\"color\":\"r\"}', '');
INSERT INTO `thistorydata` VALUES ('714', null, '2021-01-05 00:42:46', '{\"power\":\"off\"}', '');
INSERT INTO `thistorydata` VALUES ('715', null, '2021-01-05 00:42:54', '{\"power\":\"on\",\"color\":\"g\"}', '');
INSERT INTO `thistorydata` VALUES ('716', null, '2021-01-05 00:42:57', '{\"power\":\"off\"}', '');
INSERT INTO `thistorydata` VALUES ('717', null, '2021-01-05 00:55:05', '{\"temp\":13,\"humi\":46}', '');
INSERT INTO `thistorydata` VALUES ('718', null, '2021-01-05 00:55:15', '{\"temp\":14,\"humi\":45}', '');
INSERT INTO `thistorydata` VALUES ('719', null, '2021-01-05 00:55:20', '{\"temp\":14,\"humi\":46}', '');
INSERT INTO `thistorydata` VALUES ('720', null, '2021-01-05 00:55:25', '{\"temp\":14,\"humi\":47}', '');
INSERT INTO `thistorydata` VALUES ('721', null, '2021-01-05 00:55:32', '{\"temp\":14,\"humi\":45}', '');
INSERT INTO `thistorydata` VALUES ('722', null, '2021-01-05 01:00:44', '{\"power\":\"on\",\"color\":\"g\"}', '');
INSERT INTO `thistorydata` VALUES ('723', null, '2021-01-05 01:21:42', '{\"power\":\"on\",\"color\":\"g\"}', '');
INSERT INTO `thistorydata` VALUES ('724', null, '2021-01-05 01:21:43', '{\"power\":\"off\"}', '');
INSERT INTO `thistorydata` VALUES ('725', null, '2021-01-05 01:22:00', '{\"power\":\"off\"}', '');
INSERT INTO `thistorydata` VALUES ('726', null, '2021-01-05 01:22:01', '{\"power\":\"on\",\"color\":\"g\"}', '');
INSERT INTO `thistorydata` VALUES ('727', null, '2021-01-05 01:22:02', '{\"power\":\"off\"}', '');
INSERT INTO `thistorydata` VALUES ('728', null, '2021-01-05 01:24:57', '{\"power\":\"on\",\"color\":\"g\"}', '');
INSERT INTO `thistorydata` VALUES ('729', null, '2021-01-06 21:30:41', '{\"power\":\"on\",\"color\":\"r\"}', '');
INSERT INTO `thistorydata` VALUES ('730', null, '2021-01-06 21:30:44', '{\"power\":\"on\",\"color\":\"b\"}', '');

-- ----------------------------
-- Table structure for `tproject`
-- ----------------------------
DROP TABLE IF EXISTS `tproject`;
CREATE TABLE `tproject` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `uid` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `projectname` varchar(256) NOT NULL COMMENT '项目名称',
  `remark` varchar(1024) DEFAULT NULL COMMENT '备注',
  `addtime` datetime DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `ex_uid` (`uid`),
  CONSTRAINT `ex_uid` FOREIGN KEY (`uid`) REFERENCES `tuser` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='物联网监控项目';

-- ----------------------------
-- Records of tproject
-- ----------------------------
INSERT INTO `tproject` VALUES ('1', '1', '项目实例1', null, '2021-01-03 19:41:43');
INSERT INTO `tproject` VALUES ('2', '22', 'Tom\'s First Project', null, '2021-01-04 00:30:16');
INSERT INTO `tproject` VALUES ('3', '20', 'iot项目测试1', null, '2021-01-04 00:44:06');
INSERT INTO `tproject` VALUES ('4', '22', 'Tom\'s Public Project', null, '2021-01-04 19:28:44');

-- ----------------------------
-- Table structure for `tuser`
-- ----------------------------
DROP TABLE IF EXISTS `tuser`;
CREATE TABLE `tuser` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `username` varchar(128) NOT NULL COMMENT '用户名',
  `password` varchar(128) NOT NULL COMMENT '密码',
  `remark` varchar(1024) DEFAULT NULL COMMENT '备注',
  `truename` varchar(50) DEFAULT NULL COMMENT '真实姓名',
  `telephone` varchar(50) DEFAULT NULL COMMENT '电话',
  `address` varchar(150) DEFAULT NULL COMMENT '住址',
  `addtime` datetime DEFAULT NULL COMMENT '添加时间',
  `userenabled` int(11) DEFAULT '1' COMMENT '是否启用',
  `usertype` varchar(50) DEFAULT NULL COMMENT '用户类型',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Records of tuser
-- ----------------------------
INSERT INTO `tuser` VALUES ('1', 'admin', 'admin', null, null, null, null, null, '1', '管理员');
INSERT INTO `tuser` VALUES ('20', 'iot', 'iot', null, null, null, null, null, '1', '普通用户');
INSERT INTO `tuser` VALUES ('21', 'test', 'test', null, null, null, null, null, '1', '普通用户');
INSERT INTO `tuser` VALUES ('22', 'Tom', '111', null, null, null, null, null, '1', '普通用户');
INSERT INTO `tuser` VALUES ('23', 'Jerry', '111', null, null, null, null, null, '1', '普通用户');

-- ----------------------------
-- View structure for `tuserproject`
-- ----------------------------
DROP VIEW IF EXISTS `tuserproject`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `tuserproject` AS select `tproject`.`id` AS `pid`,`tproject`.`uid` AS `uid`,`tuser`.`username` AS `username`,`tuser`.`usertype` AS `usertype`,`tproject`.`projectname` AS `projectname`,`tproject`.`addtime` AS `addtime` from (`tuser` join `tproject` on((`tuser`.`id` = `tproject`.`uid`))) ;
